%% Recursive estimation.
addpath(genpath('Data'))
addpath(genpath('Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;

% Exogenus input
x94 = ptvxo94;
x95 = ptvxo95;
stem(crosscorr(x94, y94));
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))' + 1500;
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))' + 1500;
%%
ym = y94(kmod);
yv = y95(kval);         % Validation set (in season)
yt1 = y95(ktest1);      % Test set (in season)
yt2 = y94(ktest2);      % Test set (out of season)
y1 = ym(1);
y0 = ym - y1;

xm = x94(kmod);
xv = x95(kval);         % Validation set (in season)    (input)
% xt1 = x95(ktest1);      % Test set (in season)          (input)
xt2 = x94(ktest2);      % Test set (out of season)      (input)
x_mean = mean(xm);
x0 = xm - x_mean;


%% KALMAN FILTER
% Set up
load bj         % Load variables from part B

max_delay = max([length(A_armax) length(B_armax) length(C_armax)]);

x_0 = [A_armax(2:end) B_armax C_armax]';  % Initial estimate

N = length(y0);
Re = eye(length(x_0))*1e-5;
Rw = 1e-3;


% Getting the covariance matrix properly would be a PIA (pain in the ass),
% should be done if necessary, but preferably avoided.
%%
% Initial Values
Rxx_1 = eye(length(x_0))*10;
xtt_1 = zeros(size(x_0));

xsave = zeros(length(x_0), N);  % Vector to store variables
esave = zeros(N,1);            % Vector to store noises estimates
ksave = zeros(length(x_0), N);
xsave(:,1:max_delay) = kron(x_0, ones(1,max_delay));    % Add initial values

for t = max_delay:N
    Ct = [-y0(t + 1 - (2:length(A_armax))); xm(t + 1 - (1:length(B_armax))); esave(t + 1 - (1:length(C_armax)))]';                       % Time - dependent C-matrix
    esave(t) = y0(t) + [y0(t + 1 - (2:length(A_armax))); -xm(t + 1 - (1:length(B_armax))); -esave( t + 1 - (1:length(C_armax)))]'*xtt_1;  % Estimate current noise
    
    Ryy = Ct*Rxx_1*Ct' + Rw;
    Kt = Rxx_1*Ct'/Ryy;
    xtt = xtt_1 + Kt*(y0(t)-Ct*xtt_1);
    Rxx  = (eye(length(x_0)) - Kt*Ct)*Rxx_1;
   
    % Save
    xsave(:,t) = xtt;
    ksave(:,t) = Kt;
    
    % Predict
    Rxx_1 = Rxx + Re;
    xtt_1 = xtt;tempvec(1:t) = y0(1:t);
    k = 8;
    for n = 1:k
        Ct_n = [-tempvec(t + n+1- (2:max(y_index))); x0(t + n +1- (1:max(u_index))); esave(t +n+ 1 - (1:max(e_index)))]';
        tempvec(t+n) =Ct_n*xtt_1;
        p(t+n,n) = tempvec(t+n);
    end
        
end

%%
k = 1;
figure
plot(p(:,k))
hold on
plot(y0)
legend(['p' num2str(k)],'y0')

res = p(max_delay:length(y0), k);
analRes(res, 200, .05, 0);

figure
plot(xsave')

