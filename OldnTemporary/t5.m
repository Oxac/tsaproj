%% Testing to model without season
addpath(genpath('Data'))
addpath(genpath('../Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))' + 1500;
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))' + 1500;
%%
ym = y94(kmod);
yv = y95(kval);      % Validation set (in season)
yt = y95(ktest1);     % Test set (in season)
yt2 = y94(ktest2);   % Test set (out of season)
%%
figure
subplot(211)
stem([acf(ym, 400) tacf(ym ,400)]);
subplot(212)
stem(acf(ym, 400) - tacf(ym ,400));
%%
y_mean = mean(ym);
y0 = ym - mean(ym);
analRes(y0, 200);

%%
A = [1 zeros(1,25)];
C = [1 zeros(1,24)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 zeros(1,23) 1];
m_init.Structure.a.Free = [1 1 1 zeros(1,20) 1 1 1];
mod1 = pem(yd, m_init);
res = filter(mod1.a, mod1.c, yd);
res = res(40:end);
analRes(res, 200, .95, 0);
%% X-validation
% mod1 gives a better solution.
yv0 = yv - y_mean;
resX1 = filter(mod1.a, mod1.c, yv0);
resX1= resX1(40:end);
analRes(resX1, 200, .95, 0);


%% Prediction!
%%
% One training data
k = 8;
[CS, AS] = equalLength(mod1.c, mod1.a);
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
ymhat_k = filter(Gk, C, ym-y_mean)+y_mean;
ymhat_k = ymhat_k(length(Gk) + 8:end);
ymk = ym(length(Gk):end-8);

[CS, AS] = equalLength(mod1.c, mod1.a);
[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
ymhat_1 = filter(G1, C, ym-y_mean)+y_mean;
ymhat_1 = ymhat_1(length(Gk) + 1:end-7);


plot([ymhat_k ymhat_1 ymk])
legend('p_8', 'p_1', 'ym')
title('Prediction of modelling set');
%%
k = 8;
[CS, AS] = equalLength(mod1.c, mod1.a);
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
yhat_k = filter(Gk, C, yv0)+y_mean;
yhat_k = yhat_k(length(Gk) + 1:end);
yvk = yv0(length(Gk)+1:end);

[CS, AS] = equalLength(mod1.c, mod1.a);
[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
yhat_1 = filter(G1, C, yv0)+y_mean;
yhat_1 = yhat_1(length(Gk) + 1:end);


plot([yhat_k yhat_1 yvk])
legend('p_8', 'p_1', 'yv')