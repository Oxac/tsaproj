addpath(genpath('Data'))
addpath(genpath('../Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))';
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))';
%%
ym = y94(kmod);
yv = y95(kval);      % Validation set (in season)
yt = y95(ktest1);     % Test set (in season)
yt2 = y94(ktest2);   % Test set (out of season)
%%
figure
subplot(211)
stem([acf(ym, 400) tacf(ym ,400)]);
subplot(212)
stem(acf(ym, 400) - tacf(ym ,400));
%%
y_mean = mean(ym);
y0 = ym - mean(ym);
analRes(y0, 50);
% It would make sense that differentiation is not proper during winter.

%% Maybe an AR(2) process
for n = 1:50
    ar_mod = arx(y0, n);
    bicV(n)= ar_mod.Report.Fit.BIC;
    fpeV(n) = ar_mod.Report.Fit.FPE;
end
figure
subplot(121)
plot(bicV, 'x')
title('bic')
subplot(122)
plot(fpeV, 'x')
title('fpe')
%%
% might be worth including lags around 24, so let's use PEM

mod = arx(y0, 3);
res = filter(mod.a, 1, y0);
res = res(10:end);
analRes(res, 50, .95, 0);
%%
A = [1 zeros(1,25)];
C = [1 zeros(1,25)];
m_init = idpoly(A, [], C);
% m_init.Structure.a.Free = [1 1 1 1 zeros(1,19) 1 1 1];
m_init.Structure.a.Free = [1 1 1 zeros(1,21) 1 1];
% m_init.Structure.c.Free = [1 1 1 zeros(1,20) 1 1 1];
m_init.Structure.c.Free = [1 1 zeros(1,21) 1 1 1];
mod1 = pem(y0, m_init);
res = filter(mod1.a, mod1.c, y0);
res = res(40:end);
analRes(res, 200, .95, 0);
%% X-validation
% mod1 gives a better solution.
yv0 = yv - y_mean;
resX1 = filter(mod.a, mod.c, yv0);
resX2 = filter(mod1.a, mod1.c, yv0);
resX1= resX1(40:end);
resX2= resX2(40:end);
analRes(resX1, 200, .95, 0);
analRes(resX2, 200, .95, 0);

dRho = acf(resX1, 400) - acf(resX2, 400);
dPhi = pacf(resX1, 400) - pacf(resX2, 400);
figure
subplot(311)
stem(dRho)
subplot(312)
stem([acf(resX1, 400) acf(resX2, 400)])
legend('1','2')
subplot(313)
stem(dPhi)
%%
% Run t3 and test against each other. Not really a huge difference in
% correlations here.
dRho = acf(resX, 400) - acf(resX2, 400);
dPhi = pacf(resX, 400) - pacf(resX2, 400);
figure
subplot(311)
stem(dRho)
subplot(312)
stem([acf(resX, 400) acf(resX2, 400)])
legend('0','2')
subplot(313)
stem(dPhi)

%% Prediction!
%%
% One training data
k = 8;
[CS, AS] = equalLength(mod1.c, mod1.a);
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
ymhat_k = filter(Gk, C, ym-y_mean)+y_mean;
ymhat_k = ymhat_k(length(Gk) + 8:end);
ymk = ym(length(Gk):end-8);

[CS, AS] = equalLength(mod1.c, mod1.a);
[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
ymhat_1 = filter(G1, C, ym-y_mean)+y_mean;
ymhat_1 = ymhat_1(length(Gk) + 1:end-7);


plot([ymhat_k ymhat_1 ymk])
legend('p_8', 'p_1', 'ym')
title('Prediction of modelling set');
%%
k = 8;
[CS, AS] = equalLength(mod1.c, mod1.a);
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
yhat_k = filter(Gk, C, yv0)+y_mean;
yhat_k = yhat_k(length(Gk) + 1:end);
yvk = yv0(length(Gk)+1:end);

[CS, AS] = equalLength(mod1.c, mod1.a);
[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
yhat_1 = filter(G1, C, yv0)+y_mean;
yhat_1 = yhat_1(length(Gk) + 1:end);


plot([yhat_k yhat_1 yvk])
legend('p_8', 'p_1', 'yv')