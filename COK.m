%% Recursive estimation.
addpath(genpath('Data'))
addpath(genpath('Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;

% Exogenus input
x94 = ptvxo94;
x95 = ptvxo95;
%%
% Select sets
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))' + 1500;
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))' + 1500;
%%
ym = y94(kmod);
yv = y95(kval);         % Validation set (in season)
yt1 = y95(ktest1);      % Test set (in season)
yt2 = y94(ktest2);      % Test set (out of season)
y1 = ym(1);             % do this on ym for model set,
y0 = ym - y1;           % yv foor validation set and
                        % yt1 for test set etc.

xm = x94(kmod);
xv = x95(kval);         % Validation set (in season)    (input)
% xt1 = x95(ktest1);      % Test set (in season)          (input)
xt2 = x94(ktest2);      % Test set (out of season)      (input)
x_mean = mean(xm);
x0 = xm - x_mean;


%% KALMAN FILTER
% Set up
load bj         % Load variables from part B

y_index = find(A_armax);    % Output coeff, separated from 0
y_index = y_index(2:end);   % a_0 = 1, always known
u_index = find(B_armax);    % For input
e_index = find(C_armax);    % For noise

A_index = 1:length(y_index);
B_index = 1:length(u_index) + max(A_index);
C_index = 1:length(e_index) + max(B_index);
max_delay = max([y_index u_index e_index]);

x_0 = [A_armax(y_index) B_armax(u_index) C_armax(e_index)]';  % Initial estimate
% x_0 = zeros(size(x_0));
N = length(y0);
Re = eye(length(x_0))*1e-4;
Rw = 10;


% Getting the covariance matrix properly would be a PIA (pain in the ass),
% should be done if necessary, but preferably avoided.
%%
% Initial Values
Rxx_1 = eye(length(x_0))*10;
xtt_1 = x_0;

xsave = zeros(length(x_0), N);  % Vector to store variables
esave = zeros(N,1);            % Vector to store noises estimates
ksave = zeros(length(x_0), N);
xsave(:,1:max_delay) = kron(x_0, ones(1,max_delay));    % Add initial values
p = zeros(N+1,8); 
tempvec = zeros(N+8,1);

%% Run the kalman filter
for t = max_delay:N-8
    % Construct Ct, note that esave(t) = 0, so it doesn't affect anything.
    Ct = [-y0(t + 1 - [y_index]); x0(t + 1 - u_index); esave(t + 1 - e_index)]';                       % Time - dependent C-matrix
    % Estimate esave(t).
    esave(t) = y0(t) + [y0(t + 1 - y_index); -x0(t + 1 - u_index); -esave( t + 1 - e_index);]'*xtt_1;  % Estimate current noise
    
    % Calculate kalman gain
    Ryy = Ct*Rxx_1*Ct' + Rw;
    Kt = Rxx_1*Ct'/Ryy;
    xtt = xtt_1 + Kt*(y0(t)-Ct*xtt_1);
    Rxx  = (eye(length(x_0)) - Kt*Ct)*Rxx_1;
    
    % Save
    xsave(:,t) = xtt;
    ksave(:,t) = Kt;
    % Predict
    Rxx_1 = Rxx + Re;
    xtt_1 = xtt;
    
    tempvec(1:t) = y0(1:t);
    k = 8;
    for n = 1:k
        Ct_n = [-tempvec(t + n+1- y_index); x0(t + n +1- u_index); esave(t +n+ 1 - e_index)]';
        tempvec(t+n) =Ct_n*xtt_1;
        p(t+n,n) = tempvec(t+n);
    end 
end

%%
% Change k to whatever (limited in the script above) to check predictions.
k = 1;
figure
plot(p(:,k))
hold on
plot(y0)
legend(['p' num2str(k)],'y0')

res = p(1:length(y0), k) - y0;
res = res(max_delay+1:end);
analRes(res, 200, .05, 0);
%%
figure
plot(xsave')

