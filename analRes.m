function [rho, phi] = analRes(res, lags,signLvl, includeZero)
% Plots the acf, pacf, and norml
if nargin == 4
    iz = includeZero;
end
if nargin < 4
    iz = 1;
end
if nargin < 3
    signLvl = 0.95;
end
if nargin < 2
    lags = 20;
end

figure
subplot(311)
rho = acf(res, lags, signLvl, 1, [], iz);
title('acf')
subplot(312)
phi = pacf(res, lags, signLvl, 1, iz);
title('pacf')
subplot(313)
plotNTdist(res);
end

