%% This is to check that the kalman filter works properly. Time varying parameters
addpath(genpath('Data'))
addpath(genpath('Material/matlab'))
%% Simulate data
sigma2 = 4;
N = 4000;
e = sqrt(sigma2)*randn(N, 1);
w = 20*randn(N, 1);
A = conv([1 .7 .8], [1 zeros(1,23) -.95]);  
B = [1 .5];
Ca = [1 -.2 -.4];
Cb = [1 .5 .9];

Ax = conv([1 .7], [1 zeros(1,23) -.9]);
Cx = [1 -.2 .3];
%%
x = filter(Cx, Ax, w);
x = x(3:end);
e = e(3:end);
z = [filter(Ca, A, e(1:N/2));filter(Cb, A, e(floor(N/2)+1:end))];

y = filter(B, A, x) + z;
y = y(30:end);
analRes(y, 200, .05, 0);
%%
y0 = y;
x0 = x;
%% KALMAN FILTER
% Set up

y_index = find(A);    % Output coeff, separated from 0
y_index = y_index(2:end);   % a_0 = 1, always known
u_index = find(B);    % For input
e_index = find(Ca);    % For noise

A_index = 1:length(y_index);
B_index = (1:length(u_index)) + max(A_index);
C_index = (1:length(e_index)) + max(B_index);
max_delay = max([y_index u_index e_index]);

x_0 = [y_index u_index e_index]';  % Initial estimate
% x_0 = zeros(size(x_0));
x_0 = [0.72 0.56 -1 -.715 -.55 -.2 -.23 0 0 0]';
N = length(y0);
Re = eye(length(x_0))*1e-5;
Rw = 200;


% Getting the covariance matrix properly would be a PIA (pain in the ass),
% should be done if necessary, but preferably avoided.
%%
% Initial Values
Rxx_1 = eye(length(x_0))*1e-1;
xtt_1 = x_0;

xsave = zeros(length(x_0), N);  % Vector to store variables
esave = zeros(N,1);            % Vector to store noises estimates
ksave = zeros(length(x_0), N);
xsave(:,1:max_delay) = kron(x_0, ones(1,max_delay));    % Add initial values
p = zeros(N+1,8); 
tempvec = zeros(N+8,1);

%%
for t = max_delay:N-8
    
    Ct = [-y0(t + 1 - [y_index]); x0(t + 1 - u_index); esave(t + 1 - e_index)]';                       % Time - dependent C-matrix
    esave(t) = y0(t) + [y0(t + 1 - y_index); -x0(t + 1 - u_index); -esave( t + 1 - e_index);]'*xtt_1;  % Estimate current noise
    
    
    Ryy = Ct*Rxx_1*Ct' + Rw;
    Kt = Rxx_1*Ct'/Ryy;
    xtt = xtt_1 + Kt*(y0(t)-Ct*xtt_1);
    Rxx  = (eye(length(x_0)) - Kt*Ct)*Rxx_1;
    
    % Save
    xsave(:,t) = xtt;
    ksave(:,t) = Kt;
    % Predict
    Rxx_1 = Rxx + Re;
    xtt_1 = xtt;
    
    tempvec(1:t) = y0(1:t);
    k = 8;
    for n = 1:k
        Ct_n = [-tempvec(t + n+1- y_index); x0(t + n +1- u_index); esave(t +n+ 1 - e_index)]';
        tempvec(t+n) =Ct_n*xtt_1;
        p(t+n,n) = tempvec(t+n);
    end 
end

%%
k = 1;
figure
plot(p(:,k))
hold on
plot(y0)
legend(['p' num2str(k)],'y0')

res = p(1:length(y0), k) - y0;
res = res(max_delay+1:end);
analRes(res, 200, .05, 0);
%%
figure
plot(xsave(C_index,:)')
hold off
