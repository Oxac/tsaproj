%% add path
clear all 
close all
addpath(genpath('.'));
%% Load data
loadData

%% Turn off annyoing prompt
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)

%% pre processing, imputing missing data

% For the data from Sydkraft (alvesta, harplinge, svedala):
% 23:00 allways missing data
% up to approx. 10200 missing data everywhere (currently ignored)
% 14087 to 14103 missing data
% 15171 missing data
% 17456 to 17465 missing data

ava = utempAva_9395(:,3);
ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

har = utempHar_9395(:,3);
har(k') = (har(k'-1) + har(k'+1))/2;
har(end) = har(end-1);
har(15171) = (har(15171-1) + har(15171+1))/2;
har(14087:14103) = fliplr(1:(14103-14087+1)) * har(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * har(14103+1) * (1/(14103-14087+2));
har(17456:17465) = fliplr(1:(17465-17456+1)) * har(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * har(17465+1) * (1/(17465-17456+2));
harplinge = har;

sla = utempSla_9395(:,3);
sla(k') = (sla(k'-1) + sla(k'+1))/2;
sla(end) = sla(end-1);
sla(15171) = (sla(15171-1) + sla(15171+1))/2;
sla(14087:14103) = fliplr(1:(14103-14087+1)) * sla(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * sla(14103+1) * (1/(14103-14087+2));
sla(17456:17465) = fliplr(1:(17465-17456+1)) * sla(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * sla(17465+1) * (1/(17465-17456+2));
svedala = sla;

%% 

kmod = length(tid93) + (1561:3240)';
ktest = length(tid93) + (6601:8280)';






