%% add path
clear path
clear all 
close all
addpath(genpath('.'));
%% Load data
loadData

%% plot data
figure
subplot(3,2,2)
plot(ptstu93,'.')
title('Prediction of the temperature in Sturup during 1993 (SMHI)')
subplot(3,2,4)
plot(ptstu94,'.')
title('Prediction of the temperature in Sturup during 1994 (SMHI)')
subplot(3,2,6)
plot(ptstu95,'.')
title('Prediction of the temperature in Sturup during 1995 (SMHI)')
subplot(3,2,1)
plot(tstu93,'.')
title('Measured temperature in Sturup during 1993 (SMHI)')
subplot(3,2,3)
plot(tstu94,'.')
title('Measured temperature in Sturup during 1994 (SMHI)')
subplot(3,2,5)
plot(tstu95,'.')
title('Measured temperature in Sturup during 1995 (SMHI)')


figure
subplot(3,2,2)
plot(ptvxo93,'.')
title('Prediction of the temperature in V?xj? during 1993 (SMHI)')
subplot(3,2,4)
plot(ptvxo94,'.')
title('Prediction of the temperature in V?xj? during 1994 (SMHI)')
subplot(3,2,6)
plot(ptvxo95,'.')
title('Prediction of the temperature in V?xj? during 1995 (SMHI)')
subplot(3,2,1)
plot(tvxo93,'.')
title('Measured temperature in V?xj? during 1993 (SMHI)')
subplot(3,2,3)
plot(tvxo94,'.')
title('Measured temperature in V?xj? during 1994 (SMHI)')
subplot(3,2,5)
plot(tvxo95,'.')
title('Measured temperature in V?xj? during 1995 (SMHI)')

%%

% kl 23:00 finns det nollor, i.e. missing data
% fram till ca 10200 missing data lite h?r och var
% 14087 till 14103 missing data
% 15171 missing data
% 17456 till 17465 missing data

k = 24:24:(length(utempAva_9395)-1);

ava = utempAva_9395(:,3);
ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

har = utempHar_9395(:,3);
har(k') = (har(k'-1) + har(k'+1))/2;
har(end) = har(end-1);
har(15171) = (har(15171-1) + har(15171+1))/2;
har(14087:14103) = fliplr(1:(14103-14087+1)) * har(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * har(14103+1) * (1/(14103-14087+2));
har(17456:17465) = fliplr(1:(17465-17456+1)) * har(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * har(17465+1) * (1/(17465-17456+2));
harplinge = har;

sla = utempSla_9395(:,3);
sla(k') = (sla(k'-1) + sla(k'+1))/2;
sla(end) = sla(end-1);
sla(15171) = (sla(15171-1) + sla(15171+1))/2;
sla(14087:14103) = fliplr(1:(14103-14087+1)) * sla(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * sla(14103+1) * (1/(14103-14087+2));
sla(17456:17465) = fliplr(1:(17465-17456+1)) * sla(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * sla(17465+1) * (1/(17465-17456+2));
svedala = sla;


figure
subplot(3,1,1)
%plot(alvesta(6700:end),'.')
plot(alvesta(1:end),'.')
title('Measured temperature in Alvesta during 1993-95 (Sydkraft)')
subplot(3,1,2)
%plot(harplinge(6700:end),'.')
plot(harplinge(1:end),'.')
title('Measured temperature in Harplinge during 1993-95 (Sydkraft)')
subplot(3,1,3)
%plot(svedala(6700:end),'.')
plot(svedala(1:end),'.')
title('Measured temperature in Svedala during 1993-95 (Sydkraft)')



