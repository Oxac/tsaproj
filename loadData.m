load ptstu93            %  Prediction of the temperature in Sturup during 1993 (SMHI)
load ptstu94            %  Prediction of the temperature in Sturup during 1994 (SMHI)
load ptstu95            %  Prediction of the temperature in Sturup during 1995 (SMHI)

load ptvxo93            % Prediction of the temperature in V??xj?? during 1993 (SMHI)
load ptvxo94            % Prediction of the temperature in V??xj?? during 1994 (SMHI)
load ptvxo95            % Prediction of the temperature in V??xj?? during 1995 (SMHI)

load tstu93             % Measured temperature in Sturup during 1993 (SMHI)
load tstu94             % Measured temperature in Sturup during 1994 (SMHI)
load tstu95             % Measured temperature in Sturup during 1995 (SMHI)

load tvxo93             % Measured temperature in V??xj?? during 1993 (SMHI)
load tvxo94             % Measured temperature in V??xj?? during 1994 (SMHI)
load tvxo95             % Measured temperature in V??xj?? during 1995 (SMHI)

load utempAva_9395.dat  % Measured temperature in Alvesta during 1993-95 (Sydkraft)
load utempHar_9395.dat  % Measured temperature in Harplinge during 1993-95 (Sydkraft)
load utempSla_9395.dat  % Measured temperature in Svedala during 1993-95 (Sydkraft)

load tid93              %  Time schedule for the measurements for 1993
load tid94              %  Time schedule for the measurements for 1994
load tid95              %  Time schedule for the measurements for 1995