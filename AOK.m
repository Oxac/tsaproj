%% Modelling a period with less missing data!
%
addpath(genpath('Data'))
addpath(('Material/matlab'))
%% Load data
loadData
% Turn off annyoing prompt
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
% Classify data
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93)+1:end, end);

% Interpolate all missing samples at 00:00
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 24:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;
%%
% Select sets
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))' + 1500;
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))' + 1500;
%%
ym = y94(kmod);     % Modeling set
yv = y95(kval);     % Validation set (in season)
yt1 = y95(ktest1);  % Test set (in season)
yt2 = y94(ktest2);  % Test set (out of season)
%%
figure
subplot(211)
title('acf \& tacf')
stem([acf(ym, 400) tacf(ym ,400)]);
xlabel('lags')
legend('acf', 'tacf')
subplot(212)
title('acf - tacf')
stem(acf(ym, 400) - tacf(ym ,400));
xlabel('lags')
%%
y_mean = mean(ym);
y0 = ym - mean(ym);
analRes(y0, 50);
%%
s = 1;
% Delta_s = [1 zeros(1, s-1) -1];
Delta_s = 1;
yd = filter(Delta_s, 1, y0);
yd = yd(s+1:end);
analRes(yd, 200, .95, 0);


%%
A = [1 zeros(1,25)];
C = [1 zeros(1,24)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 zeros(1,21) 1 1 1];
m_init.Structure.a.Free = [1 1 1 zeros(1,20 ) 1 1 1];
mod1 = pem(yd, m_init);
res = filter(mod1.a, mod1.c, yd);
res = res(40:end);
analRes(res, 200, .95, 0);
%% X-validation
% mod1 gives a better solution.
yv0 = yv - y_mean;
resX1 = filter(conv(Delta_s,mod1.a), mod1.c, yv0);
resX1= resX1(40:end);
analRes(resX1, 200, .95, 0);

%%
% One training data
k = 8;
[CS, AS] = equalLength(mod1.c, conv(Delta_s, mod1.a));
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
ymhat_k = filter(Gk, C, ym-y_mean)+y_mean;
ymhat_k = ymhat_k(length(Gk)+1:end);
ymk = ym(length(Gk)+1:end);

[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
ymhat_1 = filter(G1, C, ym-y_mean)+y_mean;
ymhat_1 = ymhat_1(length(Gk) + 1:end);
figure
plot([ymhat_k ymhat_1 ymk])
legend('p_8', 'p_1', 'ym')
title('Prediction of modelling set');
%%
% Prediction errors
e8 = ymk - ymhat_k;
e1= ymk - ymhat_1;
% analRes(e8, 500, .95, 0);
analRes(e8, 500, .95, 0);
%%
k = 8;
[CS, AS] = equalLength(mod1.c, conv(Delta_s, mod1.a));
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);
yhat_k = filter(Gk, C, yv0)+y_mean;
yhat_k = yhat_k(length(Gk) + 1:end);
yvk = yv(length(Gk)+ 1:end);


[CS, AS] = equalLength(mod1.c, conv(Delta_s, mod1.a));
[F1, G1] = deconv(conv([1, zeros(1,0)], CS), AS);
yhat_1 = filter(G1, C, yv0)+y_mean;
yhat_1 = yhat_1(length(Gk) + 1:end);
figure
plot([yhat_k yhat_1 yvk])
legend('p_8', 'p_1', 'yv')

%%
% Residuals!
e8 = yvk - yhat_k;
e1= yvk - yhat_1;
% analRes(e8, 500, .95, 0);
analRes(e1, 500, .95, 0);
