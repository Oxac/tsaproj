addpath(genpath('.'));
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)

%% Index for data selection
kmod = length(tid93) + (1561:3240)';
kval = length(tid93) + (3241:4104)';
ktest = length(tid93) + (6601:8280)';
ava = utempAva_9395(:,3);

%% Interppolate missing samples
k = 24:24:(length(utempAva_9395)-1);

ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

%% Data selection
ym = alvesta(kmod);
yv = alvesta(kval);
yt = alvesta(ktest);

um = ptvxo94(kmod - length(tid93));
uv = ptvxo94(kval - length(tid93));
ut = ptvxo94(ktest - length(tid93));

%% Means
ym_mean = mean(ym);
um_mean = mean(um);

ym_0 = ym - ym_mean;
yv_0 = yv - ym_mean;
um_0 = um - um_mean;

%% Load previous models
% This is done to get nice initial estimates!
load TSM/arma
load TSM/BJ

D = [1 zeros(1,23) -1];
%%
% Get Arma polynomials
A_arma = arma_mod.a;
C_arma = arma_mod.c;

% Get BJ-polynomials on armax form
A_armax = MboxJ.Structure.D.Value;
B_armax = conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value);
C_armax = MboxJ.Structure.C.Value;
%% Kalman filtering: ARMA, modelling set
close all
clc

m0 = [1, A_arma(find(A_arma(2:end)) + 1) C_arma(find(C_arma(2:end)) + 1)];
V0 = arma_mod.Report.Parameters.FreeParCovariance;
V0 = blkdiag(0,V0);
sigma_e_2 = var(filter(A_arma, C_arma,filter(D, 1, ym_0)));
e_t = zeros(1, length(ym_0)+8);


% Data length
N = length(ym_0);

% Define the state space equations
A = eye(length(m0));
Re = 0.0001 * diag([0 1 1 1 1 1 1]); % Hidden state noise covariance matrix 
Rw = 100*sigma_e_2; % Observation variance


% Set some initial values
Rxx_1 = 1*V0; % Initial variance 
xtt_1 = m0'; % Initial state

% Vector to store values in
one_step_pred = zeros(1,length(ym_0));
eight_step_pred = zeros(1,length(ym_0));

% Vector to store values in
xsave=zeros(length(m0),N);

% A temp vector
y_tp = zeros(1,N+8);

% Kalman filter . Start from k=49, 
% since we need old values of y.
for k=50:N,
    
    %C is, in our case, a function of time. 
    C=[ym_0(k-24),(-ym_0(k-1)+ym_0(k-25)),(-ym_0(k-2)+ym_0(k-26)),...
        (-ym_0(k-23)+ym_0(k-47)),(-ym_0(k-24)+ym_0(k-48)),...
        e_t(k-1), e_t(k-24)];
 
    % Update
    Ryy_1 = C * Rxx_1 * C.' + Rw; 
    Kt = Rxx_1 * C.' / (Ryy_1); 
    xtt =xtt_1 + Kt * (ym_0(k) - C * xtt_1); 
    Rxx =(eye(length(m0))- Kt * C) * Rxx_1;
    
    % Save
    xsave(:,k) = xtt;
    KK(k) = norm(Kt);
    
    % Predict
    Rxx_1 = A * Rxx * A.' + Re; 
    xtt_1 = A * xtt;
    
    % Update e_t
    e_t(k) = ym_0(k) - C*xtt;
    
    % More predictions
    y_tp(1:k) = ym_0(1:k);
    for j = 1:8
        C_j = [y_tp(k-24+j),(-y_tp(k-1+j)+y_tp(k-25+j)),(-y_tp(k-2+j)+y_tp(k-26+j)),...
            (-y_tp(k-23+j)+y_tp(k-47+j)),(-y_tp(k-24+j)+y_tp(k-48+j)),...
            e_t(k-1+j), e_t(k-24+j)];
        y_t_jt = C_j*xtt;
        y_tp(k+j) = y_t_jt;
        
        if j == 1 && k <= N - j
            one_step_pred(k+j) = y_t_jt;
        elseif j == 8 && k <= N - j
            eight_step_pred(k+j) = y_t_jt;
        end
        
    end
    
     
end

one_step_pred = one_step_pred + ym_mean;
one_step_pred = one_step_pred(100:end);

eight_step_pred = eight_step_pred + ym_mean;
eight_step_pred = eight_step_pred(100:end);

ymplot = ym(100:end);

% Plot predictions and real data
figure
subplot(211)
plot([ymplot one_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data', '1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-3.5 15])
subplot(212)
plot([ymplot eight_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data','8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-3.5 15])

figure
title('Hidden statesd')
plot(xsave')
legend('1', 'a1','a2','a23','a24','c1','c24')

figure
plot(KK)
err_1 = ymplot - one_step_pred';
var(err_1)

err_8 = ymplot - eight_step_pred';
var(err_8)

%% Kalman filtering: ARMA, Validation set
close all
clc
clear KK
m0 = [1, A_arma(find(A_arma(2:end)) + 1) C_arma(find(C_arma(2:end)) + 1)];
V0 = 10000*arma_mod.Report.Parameters.FreeParCovariance;
V0 = blkdiag(0,V0);
% V0 = zeros(size(V0));
sigma_e_2 = var(filter(A_arma, C_arma,filter(D, 1, yv_0)));
e_t = zeros(1, length(yv_0)+8);


% Data length
N = length(yv_0);

% Define the state space equations
A = eye(length(m0));
Re = .0001* diag([0 1 1 1 1 1 1]); % Hidden state noise covariance matrix 
% Re = zeros(size(Re));
Rw = 100*sigma_e_2; % Observation variance

% usually C should be set here to
% but in this case C is a function of time.

% Set some initial values
Rxx_1 = 1*V0; % Initial variance 
xtt_1 = m0'; % Initial state

% Vector to store values in
one_step_pred = zeros(1,length(yv_0));
eight_step_pred = zeros(1,length(yv_0));

% Vector to store values in
xsave=zeros(length(m0),N);

% A temp vector
y_tp = zeros(1,N+8);

% Kalman filter . Start from k=49, 
% since we need old values of y.
for k=49:N,
    
    %C is, in our case, a function of time. 
    C=[yv_0(k-24),(-yv_0(k-1)+yv_0(k-25)),(-yv_0(k-2)+yv_0(k-26)),...
        (-yv_0(k-23)+yv_0(k-47)),(-yv_0(k-24)+yv_0(k-48)),...
        e_t(k-1), e_t(k-24)];
 
    % Update
    Ryy_1 = C * Rxx_1 * C.' + Rw; 
    Kt = Rxx_1 * C.' / (Ryy_1); 
    xtt =xtt_1 + Kt * (yv_0(k) - C * xtt_1); 
    Rxx =(eye(length(m0))- Kt * C) * Rxx_1;
    
    % Save
    xsave(:,k) = xtt;
    KK(k) = norm(Kt);
    
    % Predict
    Rxx_1 = A * Rxx * A.' + Re; 
    xtt_1 = A * xtt;
    
    % Update e_t
    e_t(k) = yv_0(k) - C*xtt;
    
    % More predictions
    y_tp(1:k) = yv_0(1:k);
    for j = 1:8
        C_j = [y_tp(k-24+j),(-y_tp(k-1+j)+y_tp(k-25+j)),(-y_tp(k-2+j)+y_tp(k-26+j)),...
            (-y_tp(k-23+j)+y_tp(k-47+j)),(-y_tp(k-24+j)+y_tp(k-48+j)),...
            e_t(k-1+j), e_t(k-24+j)];
        y_t_jt = C_j*xtt;
        y_tp(k+j) = y_t_jt;
        
        if j == 1 && k <= N - j
            one_step_pred(k+j) = y_t_jt;
        elseif j == 8 && k <= N - j
            eight_step_pred(k+j) = y_t_jt;
        end
        
    end
    
     
end

one_step_pred = one_step_pred + ym_mean;
one_step_pred = one_step_pred(100:end);

eight_step_pred = eight_step_pred + ym_mean;
eight_step_pred = eight_step_pred(100:end);

yvplot = yv(100:end);

% Plot predictions and real data
figure
plot([yvplot one_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data', '1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')

figure
plot([yvplot eight_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data','8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')

figure
plot(xsave')
title('Hidden states')
xlabel('t / h')
legend('1', 'a1','a2','a23','a24','c1','c24')

figure
plot(KK)
err_1 = yvplot - one_step_pred';
var(err_1)

err_8 = yvplot - eight_step_pred';
var(err_8)


%% Kalman filtering: ARMA, test set
close all
clc

yt_0 = yt - ym_mean;

m0 = [1, A_arma(find(A_arma(2:end)) + 1) C_arma(find(C_arma(2:end)) + 1)];
V0 = arma_mod.Report.Parameters.FreeParCovariance;
V0 = blkdiag(0,V0);
sigma_e_2 = var(filter(A_arma, C_arma,filter(D, 1, ym_0)));
e_t = zeros(1, length(yt_0)+8);


% Data length
N = length(yt_0);

% Define the state space equations
A = eye(length(m0));
Re = 0.0001 * diag([0 1 1 1 1 1 1]); % Hidden state noise covariance matrix 
Rw = 1000*sigma_e_2; % Observation variance

% usually C should be set here to
% but in this case C is a function of time.

% Set some initial values
Rxx_1 = 1*V0; % Initial variance 
xtt_1 = m0'; % Initial state

% Vector to store values in
one_step_pred = zeros(1,length(yt_0));
eight_step_pred = zeros(1,length(yt_0));

% Vector to store values in
xsave=zeros(length(m0),N);

% A temp vector
y_tp = zeros(1,N+8);

% Kalman filter . Start from k=49, 
% since we need old values of y.
for k=49:N,
    
    %C is, in our case, a function of time. 
    C=[yt_0(k-24),(-yt_0(k-1)+yt_0(k-25)),(-yt_0(k-2)+yt_0(k-26)),...
        (-yt_0(k-23)+yt_0(k-47)),(-yt_0(k-24)+yt_0(k-48)),...
        e_t(k-1), e_t(k-24)];
 
    % Update
    Ryy_1 = C * Rxx_1 * C.' + Rw; 
    Kt = Rxx_1 * C.' / (Ryy_1); 
    xtt =xtt_1 + Kt * (yt_0(k) - C * xtt_1); 
    Rxx =(eye(length(m0))- Kt * C) * Rxx_1;
    
    % Save
    xsave(:,k) = xtt;
    
    % Predict
    Rxx_1 = A * Rxx * A.' + Re; 
    xtt_1 = A * xtt;
    
    % Update e_t
    e_t(k) = yt_0(k) - C*xtt;
    
    % More predictions
    y_tp(1:k) = yt_0(1:k);
    for j = 1:8
        C_j = [y_tp(k-24+j),(-y_tp(k-1+j)+y_tp(k-25+j)),(-y_tp(k-2+j)+y_tp(k-26+j)),...
            (-y_tp(k-23+j)+y_tp(k-47+j)),(-y_tp(k-24+j)+y_tp(k-48+j)),...
            e_t(k-1+j), e_t(k-24+j)];
        y_t_jt = C_j*xtt;
        y_tp(k+j) = y_t_jt;
        
        if j == 1 && k <= N - j
            one_step_pred(k+j) = y_t_jt;
        elseif j == 8 && k <= N - j
            eight_step_pred(k+j) = y_t_jt;
        end
        
    end
     
end

one_step_pred = one_step_pred + ym_mean;
one_step_pred = one_step_pred(100:end);

eight_step_pred = eight_step_pred + ym_mean;
eight_step_pred = eight_step_pred(100:end);

ytplot = yt(100:end);

% Plot predictions and real data
figure
subplot(211)
plot([ytplot one_step_pred'])
title('Test set: Predictions')
legend('Measured Data', '1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-6 11.2])
subplot(212)
plot([ytplot eight_step_pred'])
title('Test set: Predictions')
legend('Measured Data','8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-6 11.2])

err_1 = ytplot - one_step_pred';
var(err_1)

err_8 = ytplot - eight_step_pred';
var(err_8)
figure
plot(xsave')
title('Hidden states')
xlabel('t / h')
legend('1', 'a1','a2','a23','a24','c1','c24')
