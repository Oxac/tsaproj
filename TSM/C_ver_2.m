addpath(genpath('.'));
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)

%% Index for data selection
kmod = length(tid93) + (1561:3240)';
kval = length(tid93) + (3241:4104)';
ktest = length(tid93) + (6601:8280)';
ava = utempAva_9395(:,3);

%% Interppolate missing samples
k = 24:24:(length(utempAva_9395)-1);

ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

%% Data selection
ym = alvesta(kmod);
yv = alvesta(kval);
yt = alvesta(ktest);

um = ptvxo94([kmod' (max(kmod):max(kmod+10))] - length(tid93));
uv = ptvxo94([kval' (max(ktest):max(ktest+10))] - length(tid93));
ut = ptvxo94([ktest' (max(ktest):max(ktest+10))] - length(tid93));

%% Means
ym_mean = mean(ym);
um_mean = mean(um);

ym_0 = ym - ym_mean;
yv_0 = yv - ym_mean;
um_0 = um - um_mean;

%% Load previous models
% This is done to get nice initial estimates!
load TSM/arma
load TSM/BJ

D = [1 zeros(1,23) -1];

% Get BJ-polynomials on armax form
A_armax = MboxJ.Structure.D.Value;
B_armax = conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value);
C_armax = MboxJ.Structure.C.Value;
%% Kalman filtering: ARMA, modelling set
close all
clc

x0 = [A_armax(find(A_armax)) B_armax(find(B_armax)) C_armax(find(C_armax(2:end)) + 1)];

V0 = 0.0001*eye(length(x0));
%V0 = zeros(size(V0));

e_t = zeros(1, length(ym_0)+8);

N = length(ym_0);

% Define the state space equations
A = eye(length(x0));
Re = 0.0001 * diag([0 1 1 1 1 1 1 1 1 1 1]); % Hidden state noise covariance matrix 

% Re = zeros(size(Re));

Rw = 30; % Observation variance

% Set some initial values
Rxx_1 = 1*V0; % Initial variance 
xtt_1 = x0'; % Initial state

% Vector to store values in
one_step_pred = zeros(1,length(ym_0));
eight_step_pred = zeros(1,length(ym_0));

% Vector to store values in
xsave=zeros(length(x0),N);

% A temp vector
y_tp = zeros(1,N+8);

% Kalman filter . Start from k=49, 
% since we need old values of y.

% Kalman filter . Start from k=49, 
% since we need old values of y.
for k=50:N,
    
    %C is, in our case, a function of time. 
    C=[ym_0(k-24),(-ym_0(k-1)+ym_0(k-25)),(-ym_0(k-2)+ym_0(k-26)),...
        (-ym_0(k-23)+ym_0(k-47)),(-ym_0(k-24)+ym_0(k-48)),...
        (um_0(k-1) - um_0(k-25)), (um_0(k-2) - um_0(k-26)),...
        (um_0(k-3) - um_0(k-27)), (um_0(k-24) - um_0(k-48)),...
        (um_0(k-25) - um_0(k-49)), e_t(k-24)];
 
    % Update
    Ryy_1 = C * Rxx_1 * C.' + Rw; 
    Kt = Rxx_1 * C.' / (Ryy_1); 
    xtt =xtt_1 + Kt * (ym_0(k) - C * xtt_1); 
    Rxx =(eye(length(x0))- Kt * C) * Rxx_1;
    
    % Save
    xsave(:,k) = xtt;
    KK(k) = norm(Kt);
    
    % Predict
    Rxx_1 = A * Rxx * A.' + Re; 
    xtt_1 = A * xtt;
    
    % Update e_t
    e_t(k) = ym_0(k) - C*xtt;
    
    % More predictions
    y_tp(1:k) = ym_0(1:k);
    for j = 1:8
        C_j = [y_tp(k-24+j),(-y_tp(k-1+j)+y_tp(k-25+j)),(-y_tp(k-2+j)+y_tp(k-26+j)),...
            (-y_tp(k-23+j)+y_tp(k-47+j)),(-y_tp(k-24+j)+y_tp(k-48+j)),...
            (um_0(k+j-1) - um_0(k+j-25)), (um_0(k+j-2) - um_0(k+j-26)),...
            (um_0(k+j-3) - um_0(k+j-27)), (um_0(k+j-24) - um_0(k+j-48)),...
            (um_0(k+j-25) - um_0(k+j-49)), e_t(k-24+j)];
        y_t_jt = C_j*xtt;
        y_tp(k+j) = y_t_jt;
        
        if j == 1 && k <= N - j
            one_step_pred(k+j) = y_t_jt;
        elseif j == 8 && k <= N - j
            eight_step_pred(k+j) = y_t_jt;
        end
        
    end
end


one_step_pred = one_step_pred + ym_mean;
one_step_pred = one_step_pred(100:end);

eight_step_pred = eight_step_pred + ym_mean;
eight_step_pred = eight_step_pred(100:end);

ymplot = ym(100:end);

% Plot predictions and real data
figure
subplot(211)
plot([ymplot one_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data', '1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-3.5 15])
subplot(212)
plot([ymplot eight_step_pred'])
title('Modelling set: Predictions')
legend('Measured Data','8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-3.5 15])

figure
title('Hidden statesd')
plot(xsave')
legend('1', 'a1','a2','a23','a24','c1','c24')

figure
plot(KK)
err_1 = ymplot - one_step_pred';
var(err_1)

err_8 = ymplot - eight_step_pred';
var(err_8)


%% Kalman filtering: BJ, test set
yt_0 = yt - ym_mean;
ut_0 = ut - um_mean;
close all
clc

x0 = [A_armax(find(A_armax)) B_armax(find(B_armax)) C_armax(find(C_armax(2:end)) + 1)];

V0 = eye(length(x0));


e_t = zeros(1, length(yt_0)+8);

N = length(yt_0);

% Define the state space equations
A = eye(length(x0));
Re = 0.0001 * diag([0 1 1 1 1 1 1 1 1 1 1]); % Hidden state noise covariance matrix 

% Re = zeros(size(Re));

Rw = 30; % Observation variance

% Set some initial values
Rxx_1 = 1*V0; % Initial variance 
xtt_1 = x0'; % Initial state

% Vector to store values in
one_step_pred = zeros(1,length(yt_0));
eight_step_pred = zeros(1,length(yt_0));

% Vector to store values in
xsave=zeros(length(x0),N);

% A temp vector
y_tp = zeros(1,N+8);

% Kalman filter . Start from k=49, 
% since we need old values of y.

% Kalman filter . Start from k=49, 
% since we need old values of y.
for k=50:N,
    
    %C is, in our case, a function of time. 
    C=[yt_0(k-24),(-yt_0(k-1)+yt_0(k-25)),(-yt_0(k-2)+yt_0(k-26)),...
        (-yt_0(k-23)+yt_0(k-47)),(-yt_0(k-24)+yt_0(k-48)),...
        (ut_0(k-1) - ut_0(k-25)), (ut_0(k-2) - ut_0(k-26)),...
        (ut_0(k-3) - ut_0(k-27)), (ut_0(k-24) - ut_0(k-48)),...
        (ut_0(k-25) - ut_0(k-49)), e_t(k-24)];
 
    % Update
    Ryy_1 = C * Rxx_1 * C.' + Rw; 
    Kt = Rxx_1 * C.' / (Ryy_1); 
    xtt =xtt_1 + Kt * (yt_0(k) - C * xtt_1); 
    Rxx =(eye(length(x0))- Kt * C) * Rxx_1;
    
    % Save
    xsave(:,k) = xtt;
    KK(k) = norm(Kt);
    
    % Predict
    Rxx_1 = A * Rxx * A.' + Re; 
    xtt_1 = A * xtt;
    
    % Update e_t
    e_t(k) = yt_0(k) - C*xtt;
    
    % More predictions
    y_tp(1:k) = yt_0(1:k);
    for j = 1:8
        C_j = [y_tp(k-24+j),(-y_tp(k-1+j)+y_tp(k-25+j)),(-y_tp(k-2+j)+y_tp(k-26+j)),...
            (-y_tp(k-23+j)+y_tp(k-47+j)),(-y_tp(k-24+j)+y_tp(k-48+j)),...
            (ut_0(k+j-1) - ut_0(k+j-25)), (ut_0(k+j-2) - ut_0(k+j-26)),...
            (ut_0(k+j-3) - ut_0(k+j-27)), (ut_0(k+j-24) - ut_0(k+j-48)),...
            (ut_0(k+j-25) - ut_0(k+j-49)), e_t(k-24+j)];
        y_t_jt = C_j*xtt;
        y_tp(k+j) = y_t_jt;
        
        if j == 1 && k <= N - j
            one_step_pred(k+j) = y_t_jt;
        elseif j == 8 && k <= N - j
            eight_step_pred(k+j) = y_t_jt;
        end
        
    end
end


one_step_pred = one_step_pred + ym_mean;
one_step_pred = one_step_pred(100:end);

eight_step_pred = eight_step_pred + ym_mean;
eight_step_pred = eight_step_pred(100:end);

ytplot = yt(100:end);

% Plot predictions and real data
figure
subplot(211)
plot([ytplot one_step_pred'])
title('Test set: Predictions')
legend('Measured Data', '1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-6.5 14])
subplot(212)
plot([ytplot eight_step_pred'])
title('Test set: Predictions')
legend('Measured Data','8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
xlim([750 1000])
ylim([-6.5 14])

figure
plot(KK)
err_1 = ytplot - one_step_pred';
var(err_1)

err_8 = ytplot - eight_step_pred';
var(err_8)
