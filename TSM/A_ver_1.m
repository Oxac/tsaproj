addpath(genpath('.'));
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
% Tr??ning
% 1561 - 1994; 10 1 0
% 3240 - 1994; 19 7 23

% Validering
% 4104 - 1994; 24 7 23

% Test
% 6601 - 1994; 40 1 0
% 8280 - 1994; 49 7 23

kmod = length(tid93) + (1561:3240)';
kval = length(tid93) + (3241:4104)';
ktest = length(tid93) + (6601:8280)';
ava = utempAva_9395(:,3);

%% Interppolate missing samples

k = 24:24:(length(utempAva_9395)-1);

ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

%% Data selection
ym = alvesta(kmod);
yv = alvesta(kval);
yt = alvesta(ktest);

subplot(311)
plot(ym)
title('Modelling set')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
subplot(312)
plot(yv)
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
title('Validation set')
subplot(313)
plot(yt)
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
title('Test set')

%%
figure
subplot(211)
stem([acf(ym, 100) tacf(ym ,100)]);
title('Modelling set: acf & tacf')
xlabel('lags')
legend('acf', 'tacf')
subplot(212)
stem(acf(ym, 100) - tacf(ym ,100));
title('Modelling set: (acf - tacf)')
xlabel('lags')

%%
ymean = mean(ym);
y0 = ym - ymean;
figure
plot(y0)
analRes(ym, 100);
%%
D1 = [1 -1];
D1 = 1;
Ds = [1 zeros(1, 23) -1];
D = conv(D1, Ds);


ys = filter(D, 1, y0);
ys = ys(25:end);
analRes(ys, 100);

%%
A = [1 zeros(1,24)];
C = [1 zeros(1,24)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 1 zeros(1,22) 1];
m_init.Structure.a.Free = [1 1 1 zeros(1,20) 1 1];
mod1 = pem(ys, m_init); 
clc
present(mod1)

%%
res = filter(mod1.a, mod1.c, ys);
analRes(res,50,0.05, 0);

%%
whitenessTest(res)

%% X - validation
yv0 = filter(D, 1, yv - ymean);
resX = filter(mod1.a, mod1.c, yv0);
analRes(resX,50,0.05, 0);
figure
whitenessTest(resX)
%% 1 step prediction
[CS,AS] = equalLength( mod1.c, conv(mod1.a, D) );
k = 1;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], CS ), AS );

y_1_hat = filter( Gk_y, mod1.c, y0) + ymean;
err1 = ym - y_1_hat;
figure
subplot(211)
plot([y_1_hat ym])
title('Original domain')
legend('1 step pred', 'modelling data')
subplot(212)
plot(err1)
title('diff')
analRes(err1, 50, .05, 0);
%% 8 step prediction

[CS,AS] = equalLength( mod1.c, conv(mod1.a, D) );
k = 8;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], CS ), AS );

y_8_hat = filter( Gk_y, mod1.c, y0) + ymean;
err8 = ym - y_8_hat;

figure
subplot(211)
plot([y_8_hat ym])
title('Original domain')
legend('8 step pred', 'modelling data')
subplot(212)
plot(err8)
title('diff')

analRes(err8, 50, .05, 0);
%%
% Throw values
y_1_hat = y_1_hat(100:end);
ymplot = ym(100:end);
y_8_hat = y_8_hat(100:end);
% Get benchmarks:
benchm1 = ymplot(1:end-1);
benchm8 = ymplot(1:end-24);
%% Plot nicely
% Plot predictions and real data
figure
subplot(211)
plot([ymplot y_1_hat])
xlim([750 1000])
title('Modelling set: Predictions')
legend('Measured Data','1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')

subplot(212)
plot([ymplot y_8_hat])
xlim([750 1000])
title('Modelling set: Predictions')
legend('Measured Data', '8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')

figure
subplot(211)
plot([ymplot(2:end) y_1_hat(2:end) benchm1])
xlim([750 1000])
title('Modelling set: 1 step vs Benchmark')
legend('Measured data','1-step', 'benchmark');
xlabel('t / h')
ylabel('T / ^{\circ}C')
subplot(212)
plot([ymplot(24+1:end) y_8_hat(24+1:end) benchm8])
xlim([750 1000])
title('Modelling Set: 8 step vs Benchmark')
legend('Measured data','8-step', 'benchmark');
xlabel('t / h')
ylabel('T / ^{\circ}C')
%% Compute variances
errm1 = ymplot(2:end) - y_1_hat(2:end);
benchErrm1 = ymplot(2:end) - benchm1;
errm8 = ymplot(25:end) - y_8_hat(25:end);
benchErrm8 = ymplot(25:end) - benchm8;

var(errm1)
var(benchErrm1)
var(errm8)
var(benchErrm8)


%% Redo on the test set
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
%% 1 step prediction

[CS,AS] = equalLength( mod1.c, conv(mod1.a, D) );
k = 1;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], CS ), AS );

yt_1_hat = filter( Gk_y, mod1.c, yt-ymean) + ymean;
errt1 = yt - yt_1_hat;
figure
subplot(211)
plot([yt_1_hat yt])
title('Original domain')
legend('1 step pred', 'modelling data')
subplot(212)
plot(errt1)
title('diff')
analRes(errt1, 50, .05,0);
%% 8 step prediction

[CS,AS] = equalLength( mod1.c, conv(mod1.a, D) );
k = 8;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], CS ), AS );

yt_8_hat = filter( Gk_y, mod1.c, yt-ymean) + ymean;
errt8 = yt - yt_8_hat;

figure
subplot(211)
plot([yt_8_hat yt])
title('Original domain')
legend('8 step pred', 'modelling data')
subplot(212)
plot(err8)
title('diff')

analRes(errt8, 50, .05,0);

%%
% Throw values
yt_1_hat = yt_1_hat(100:end);
ytplot = yt(100:end);
yt_8_hat = yt_8_hat(100:end);
% Get benchmarks:
bencht1 = ytplot(1:end-1);
bencht8 = ytplot(1:end-24);
%% Plot nicely
% Plot predictions and real data
figure
subplot(211)
plot([ytplot yt_1_hat])
xlim([750 1000])
ylim([-7 13])
% axis tight
title('Test set: Predictions')
xlabel('t / h')
ylabel('T / ^{\circ}C')
legend('Measured Data', '1-step')

subplot(212)
plot([ytplot yt_8_hat])
xlim([750 1000])
ylim([-7 13])
% axis tight
title('Test set: Predictions')
xlabel('t / h')
ylabel('T / ^{\circ}C')
legend('Measured Data', '8-step')

figure
subplot(211)
plot([ytplot(2:end) yt_1_hat(2:end) bencht1])
xlim([750 1000])
title('Test set: 1 step vs Benchmark')
legend('Measured data','1-step', 'benchmark');
xlabel('t / h')
ylabel('T / ^{\circ}C')

subplot(212)
plot([ytplot(24+1:end) yt_8_hat(24+1:end) bencht8])
xlim([750 1000])
title('Test set: 8 step vs Benchmark')
legend('Measured data','8-step', 'benchmark');
xlabel('t / h')
ylabel('T / ^{\circ}C')
%% Compute variances
errt1 = ytplot(2:end) - yt_1_hat(2:end);
benchErrt1 = ytplot(2:end) - bencht1;
errt8 = ytplot(25:end) - yt_8_hat(25:end);
benchErrt8 = ytplot(25:end) - bencht8;

var(errt1)
var(benchErrt1)
var(errt8)
var(benchErrt8)

%% Save model!
arma_mod = mod1;
save('TSM/arma','arma_mod')