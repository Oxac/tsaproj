addpath(genpath('.'));
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%% Index for data selection
kmod = length(tid93) + (1561:3240)';
kval = length(tid93) + (3241:4104)';
ktest = length(tid93) + (6601:8280)';
ava = utempAva_9395(:,3);

%% Interppolate missing samples
k = 24:24:(length(utempAva_9395)-1);

ava(k') = (ava(k'-1) + ava(k'+1))/2;
ava(end) = ava(end-1);
ava(15171) = (ava(15171-1) + ava(15171+1))/2;
ava(14087:14103) = fliplr(1:(14103-14087+1)) * ava(14087-1) * (1/(14103-14087+2)) + ...
    (1:(14103-14087+1)) * ava(14103+1) * (1/(14103-14087+2));
ava(17456:17465) = fliplr(1:(17465-17456+1)) * ava(17456-1) * (1/(17465-17456+2)) + ...
    (1:(17465-17456+1)) * ava(17465+1) * (1/(17465-17456+2));
alvesta = ava;

%% Data selection
ym = alvesta(kmod);
yv = alvesta(kval);
yt = alvesta(ktest);

um = ptvxo94(kmod - length(tid93));
uv = ptvxo94(kval - length(tid93));
ut = ptvxo94(ktest - length(tid93));

figure
subplot(311)
plot(ym)
title('Modelling set: Alvesta')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
subplot(312)
plot(yv)
title('Validation set: Alvesta')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
subplot(313)
plot(yt)
title('Test set: Alvesta')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight

figure
subplot(311)
plot(um)
title('Modelling set: Växjö')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
subplot(312)
plot(uv)
title('Validation set: Växjö')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight
subplot(313)
plot(ut)
title('Test set: Växjö')
xlabel('t / h')
ylabel('T / ^{\circ}C')
axis tight

%% Means
ym_mean = mean(ym);
um_mean = mean(um);

ym_0 = ym - ym_mean;
um_0 = um - um_mean;

%% FIND ARMAX
%First, ARMA model for um_0

close all

figure
lags = 50;
subplot(311)
acf_um_0 = acf(um_0, lags);
stem(0:lags, acf_um_0)
subplot(312)
pacf_um_0 = pacf(um_0, lags);
stem(0:lags, pacf_um_0)
subplot(313)
normplot(um_0)

%%
figure
subplot(211)
stem([acf(um_0, 400) tacf(um_0 ,400)]);
subplot(212)
stem(acf(um_0, 400) - tacf(um_0 ,400));

%% 
D = [1 zeros(1, 23) -1];
um_0_s = filter(D, 1, um_0);
um_0_s = um_0_s(25:end);

figure
lags = 50;
subplot(311)
acf(um_0_s, lags, 0.05, 1, 0, 0);
subplot(312)
pacf(um_0_s, lags, 0.05, 1, 0);
subplot(313)
normplot(um_0_s)

%%
figure
subplot(211)
stem([acf(um_0_s, 400) tacf(um_0_s ,400)]);
subplot(212)
stem(acf(um_0_s, 400) - tacf(um_0_s ,400));

figure 
plot(um_0_s)

%% trim outliers IGNORE
%sorted = sort(um_0_s);
%upper_index = floor(length(sorted)*0.93);
%lower_index = floor(length(sorted)*0.07);
%
%um_0_s_trim = um_0_s;
%um_0_s_trim(um_0_s >= sorted(upper_index)) = sorted(upper_index);
%um_0_s_trim(um_0_s <= sorted(lower_index)) = sorted(lower_index);
%
%% IGNORE
%figure
%subplot(211)
%stem([acf(um_0_s_trim, 400) tacf(um_0_s_trim ,400)]);
%subplot(212)
%stem(acf(um_0_s_trim, 400) - tacf(um_0_s_trim ,400));
%
%figure 
%plot(um_0_s_trim)
%
%
%
%%
clc
A = [1 zeros(1,25)];
C = [1 zeros(1,25)];
m_init = idpoly(A, [], C);
%                          0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
m_init.Structure.c.Free = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1];
m_init.Structure.a.Free = [1 1 0 1 1 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1];
mod_um_0_s = pem(um_0_s, m_init);
present(mod_um_0_s)
um_0_s_pw = filter(mod_um_0_s.a, mod_um_0_s.c, um_0_s);
um_0_s_pw = um_0_s_pw(40:end);
analRes(um_0_s_pw, 40, .05, 0);

figure
whitenessTest(um_0_s_pw)

%%
% Now, find d,r,s

D = [1 zeros(1, 23) -1];
ym_0_s = filter(D, 1, ym_0);
ym_0_s = ym_0_s(25:end);

ym_0_s_pw = filter(mod_um_0_s.a, mod_um_0_s.c, ym_0_s);
ym_0_s_pw = ym_0_s_pw(40:end);

M= 50;
figure
stem(-M:M, crosscorr(um_0_s_pw,ym_0_s_pw, M));
title('Cross correlation function')
xlabel('Lag')
hold on
plot(-M:M, 2/sqrt(length(um_0_s_pw))*ones(1,2*M+1), '--') 
plot(-M:M, -2/sqrt(length(um_0_s_pw))*ones(1,2*M+1), '--') 
hold off 

%%
clc
A2 = [1];
B = [0 1];
Mi = idpoly ([1] , [B] ,[] ,[] , [A2]);
z_pw = iddata(ym_0_s_pw, um_0_s_pw);
Mba2 = pem(z_pw, Mi); 
present(Mba2)
vhat = resid(Mba2, z_pw);

figure
whitenessTest(vhat.y)

figure
M = 50;
stem(-M:M, crosscorr(um_0_s_pw,vhat.y, M));
title('Cross correlation function')
xlabel('Lag')
hold on
plot(-M:M, 2/sqrt(length(um_0_s_pw))*ones(1,2*M+1), '--') 
plot(-M:M, -2/sqrt(length(um_0_s_pw))*ones(1,2*M+1), '--') 
hold off

% So, d = 1 (delay), r = 0 (A_2), s = 0 (B)

%%

% Now, p and q.

x = ym_0_s - filter(Mba2.B, Mba2.F, um_0_s);
x = x(50:end);
figure
M = 50;
stem(-M:M, crosscorr(x, um_0_s(50:end), M));
title('Cross correlation function')
xlabel('Lag')
hold on
plot(-M:M, 2/sqrt(length(x))*ones(1,2*M+1), '--') 
plot(-M:M, -2/sqrt(length(x))*ones(1,2*M+1), '--') 
hold off

figure
subplot(411)
plot(x)
subplot(412)
acf(x, 40, 0.05, 1, 0, 0);
subplot(413)
pacf(x, 40, 0.05, 1, 0);
subplot(414)
normplot(x)

%%
clc
figure
arma_model = armax(x, [25 2]);
present(arma_model)
x_pw = filter(arma_model.A, arma_model.C, x);
x_pw = x_pw(101:end);
subplot(311)
acf(x_pw, 40, 0.05, 1, 0, 0);
subplot(312)
pacf(x_pw, 40, 0.05, 1, 0);
subplot(313)
normplot(x_pw)

figure
whitenessTest(x_pw)

% Thus, p = 25 and q = 2 or 25 ???!?!?!?!!?

%%
% Finally, full estimation.

% p = 25 (A_1)
% q = 2 (C) or 25 ?!?!?!?!
% d = 1 delay
% s = 0 (B)
% r = 0 (A_2)
clc
A1= [1 zeros(1, 25)];
A2= [1];
B= [0 1];
C= [1 zeros(1, 25)];
Mi = idpoly(1, B, C, A1, A2);
%                      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
Mi.Structure.D.Free = [1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0];
Mi.Structure.C.Free = [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0];
z = iddata(ym_0_s, um_0_s);
MboxJ = pem(z, Mi); 
present(MboxJ)
ehat = resid(MboxJ,z);
ehat = ehat.y(101:end);


analRes(ehat, 50, .05, 0);

figure
whitenessTest(ehat)

%% Cross-validation 
clc
close all
yv_mean = mean(yv);
uv_mean = mean(uv);

yv_0 = yv - yv_mean;
uv_0 = uv - uv_mean;

D = [1 zeros(1, 23) -1];

yv_0_s = filter(D, 1, yv_0);
yv_0_s = yv_0_s(25:end);

uv_0_s = filter(D, 1, uv_0);
uv_0_s = uv_0_s(25:end);

z = iddata(yv_0_s, uv_0_s);
ehat = resid(MboxJ,z);
ehat = ehat.y(101:end);


analRes(ehat, 60, .05, 0);

figure
whitenessTest(ehat)

%% Now predition 1-step on modelling data
%convert to ARMAX from
A = conv(MboxJ.Structure.D.Value, D);
B = conv(conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value),D);
C = MboxJ.Structure.C.Value;

[C,A] = equalLength(C, A);
k = 1;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], C ), A );
BFk_y = conv(B, Fk_y);
[BFk_yS,C] = equalLength( BFk_y, C );
[Fk_u, Gk_u] = deconv( conv( [1,zeros(1,k-1)], BFk_yS ), C );

u_hat = filter( Gk_u, C, um_0 ) + filter(Fk_u, 1, um_0);
y_1_hat = filter( Gk_y, C, ym_0 );

y_1_hat = u_hat + y_1_hat + ym_mean;
y_1_hat = y_1_hat(100:end);


err1 = ym(100:end) - y_1_hat;
figure
subplot(211)
plot([y_1_hat ym(100:end)])
title('Original domain')
legend('1 step pred', 'modelling data')
subplot(212)
plot(err1)
title('diff')
analRes(err1, 50, .05, 0);

var(err1)

%% Now predition 8-step on modelling data
%convert to ARMAX from 
close all

A = conv(MboxJ.Structure.D.Value, D);
B = conv(conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value),D);
C = MboxJ.Structure.C.Value;

[C,A] = equalLength(C, A);
k = 8;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], C ), A );
BFk_y = conv(B, Fk_y);
[BFk_yS,C] = equalLength( BFk_y, C );
[Fk_u, Gk_u] = deconv( conv( [1,zeros(1,k-1)], BFk_yS ), C );

u_hat = filter( Gk_u, C, um_0 ) + filter(Fk_u, 1, um_0);
y_8_hat = filter( Gk_y, C, ym_0 );

y_8_hat = u_hat + y_8_hat + ym_mean;
y_8_hat = y_8_hat(100:end);


err8 = ym(100:end) - y_8_hat;
figure
subplot(211)
plot([y_8_hat ym(100:end)])
title('Original domain')
legend('1 step pred', 'modelling data')
subplot(212)
plot(err8)
title('diff')
analRes(err8, 50, .05, 0);

var(err8)

%% Now predition 1-step on test data
%convert to ARMAX from
A = conv(MboxJ.Structure.D.Value, D);
B = conv(conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value),D);
C = MboxJ.Structure.C.Value;

[C,A] = equalLength(C, A);
k = 1;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], C ), A );
BFk_y = conv(B, Fk_y);
[BFk_yS,C] = equalLength( BFk_y, C );
[Fk_u, Gk_u] = deconv( conv( [1,zeros(1,k-1)], BFk_yS ), C );

ut_hat = filter( Gk_u, C, ut-um_mean) + filter(Fk_u, 1, ut-um_mean);
yt_1_hat = filter( Gk_y, C, yt-ym_mean );

yt_1_hat = ut_hat + yt_1_hat + ym_mean;
yt_1_hat = yt_1_hat(100:end);


errt1 = yt(100:end) - yt_1_hat;
figure
subplot(211)
plot([yt_1_hat yt(100:end)])
title('Original domain')
legend('1 step pred', 'modelling data')
subplot(212)
plot(errt1)
title('diff')
analRes(errt1, 50, .05, 0);

var(errt1)

%% Now predition 8-step on test data
%convert to ARMAX from

close all

A = conv(MboxJ.Structure.D.Value, D);
B = conv(conv(MboxJ.Structure.D.Value, MboxJ.Structure.B.Value),D);
C = MboxJ.Structure.C.Value;

[C,A] = equalLength(C, A);
k = 8;
[Fk_y, Gk_y] = deconv( conv( [1,zeros(1,k-1)], C ), A );
BFk_y = conv(B, Fk_y);
[BFk_yS,C] = equalLength( BFk_y, C );
[Fk_u, Gk_u] = deconv( conv( [1,zeros(1,k-1)], BFk_yS ), C );

ut_hat = filter( Gk_u, C, ut-um_mean) + filter(Fk_u, 1, ut-um_mean);
yt_8_hat = filter( Gk_y, C, yt-ym_mean  );

yt_8_hat = ut_hat + yt_8_hat + ym_mean;
yt_8_hat = yt_8_hat(100:end);


errt8 = yt(100:end) - yt_8_hat;
figure
subplot(211)
plot([yt_8_hat yt(100:end)])
title('Original domain')
legend('8 step pred', 'modelling data')
subplot(212)
plot(errt8)
title('diff')
analRes(errt8, 50, .05, 0);

var(errt8)


%% Plots!
% Modelling sets:
ymplot = ym(100:end);
% Plot predictions and real data
figure
plot([ymplot y_1_hat y_8_hat])
xlim([750 1000])
% axis tight
title('Modelling set: Predictions')
legend('Measured Data', '1-step', '8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')


% Test sets:
ytplot = yt(100:end);
% Plot predictions and real data
figure
plot([ytplot yt_1_hat yt_8_hat])
xlim([750 1000])
% axis tight
title('Test set: Predictions')
legend('Measured Data', '1-step', '8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
%%
close all
figure
subplot(211)
plot([ymplot y_1_hat])
xlim([750 1000])
title('Modelling set: Predictions')
legend('Measured Data','1-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')

subplot(212)
plot([ymplot y_8_hat])
xlim([750 1000])
title('Modelling set: Predictions')
legend('Measured Data', '8-step')
xlabel('t / h')
ylabel('T / ^{\circ}C')
%%
figure
subplot(211)
plot([ytplot yt_1_hat])
xlim([750 1000])
ylim([-7 13])
% axis tight
title('Test set: Predictions')
xlabel('t / h')
ylabel('T / ^{\circ}C')
legend('Measured Data', '1-step')

subplot(212)
plot([ytplot yt_8_hat])
xlim([750 1000])
ylim([-7 13])
% axis tight
title('Test set: Predictions')
xlabel('t / h')
ylabel('T / ^{\circ}C')
legend('Measured Data', '8-step')
save('TSM/BJ','MboxJ')
