%% Test with \Delta, doesn't give any better results I think.
addpath(genpath('Data'))
addpath(genpath('../Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))';
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))';
%%
ym = y94(kmod);
yv = y95(kval);      % Validation set (in season)
yt = y95(ktest1);     % Test set (in season)
yt2 = y94(ktest2);   % Test set (out of season)
%%
y_mean = mean(ym);
y0 = ym - mean(ym);
analRes(y0, 50);
%%
% Differentiate
s = 24;
Delta = [1 -1];
% Delta = 1;
Delta_s = [1 zeros(1, s-1) -1];
yd = filter(conv(Delta, Delta_s), 1, y0);
yd = yd(s+2:end);
analRes(yd, 50, .95, 0);
%%
A = [1 zeros(1,25)];
C = [1 zeros(1,24)];
mi = idpoly(A, [], C);
mi.Structure.c.Free = [1 zeros(1,22) 1 1];
mi.Structure.a.Free = [1 1 1 zeros(1,21) 1 1];
mod = pem(yd, mi);

res = filter(mod.a, mod.c, yd);
present(mod)
analRes(res, 50, .95, 0);

%%
A = [1 1 1];
C = [1 zeros(1,24)];
mi = idpoly(A, [], C);
mi.Structure.c.Free = [1 zeros(1,22) 1 1];
% mi.Structure.a.Free = [1 1 1 zeros(1,21) 1 1];
mod = pem(yd, mi);

res = filter(mod.a, mod.c, yd);
present(mod)
analRes(res, 500, .95, 0);


%%
% X-validation

yv0 = yv - y_mean;
resX = filter(conv(mod.a, conv(Delta_s, Delta)), mod.c, yv0);
resX = resX(40:end);
analRes(resX, 200, .95, 0);