addpath(genpath('Data'))
addpath(genpath('../Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
plot(tstu94,'*')
y = tstu94;
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))';
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))';
%%
ym = tstu94(kmod);
yv = tstu95(kval);      % Validation set (in season)
yt1 = tstu95(ktest1);     % Test set (in season)
yt2 = tstu94(ktest2);   % Test set (out of season)

%%
subplot(211)
stem([acf(y0, 50) tacf(y0, 50)])
title(' Acf and tAcf')
legend('ACF','TACF')
subplot(212)
stem(acf(y0, 50) - tacf(y0, 50))
title('ACF - TACF')
%%
% Seems to be no reason to handle outliers...
% Check Box-Cox normality plot
lam1 = bcNormPlot(tstu94);
lam2 = bcNormPlot(ym-min(ym));
%%
% Completely different transformations... So lets not do a transformation
% for now.
%%
yt = ((ym - min(ym)+1).^lam1-1)/lam1;
% yt = ((ym - min(ym)+1).^lam2-1)/lam2;
% yt = ym;
y_mean = mean(yt);
yt = yt - mean(yt);
analRes(yt, 50);
%%
% test differentiating.
s = 24;
As = [1 zeros(1, s-1) -1];
ys = filter(As, 1, yt);
ys = ys(s + 1:end);
plot(ys)
analRes(ys, 50);
%%
% Maybe an AR(2) process
for n = 1:12
    ar_mod = arx(ys, n);
    bicV(n)= ar_mod.Report.Fit.BIC;
    fpeV(n) = ar_mod.Report.Fit.FPE;
end
figure
subplot(121)
plot(bicV)
title('bic')
subplot(122)
plot(fpeV)
title('fpe')
%%
% Filter w. AR(2) and test.
ar_mod = arx(ys, 2);
e_ar =  filter(ar_mod.a, 1, ys);
e_ar = e_ar(3:end);
analRes(e_ar, 50, .95, 0);

%%
% Try adding MA part from lag 3 and 24

A = [1 0 0];
C = [1 zeros(1, 24)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 1 1 1 zeros(1, 23 - 3) 1];
mod1 = pem(ys, m_init);
mod2 = pem(ym - mean(ym), m_init); %    Gives best result. Maybe don't do differentiation.

res = filter(mod2.a, mod2.c, ym - mean(ym));
present(mod2)
analRes(res, 50, .95, 0);