\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\graphicspath{ {./Img/} }
\usepackage{epstopdf}
\usepackage{float}
\usepackage[labelfont=bf, margin=0.25\textwidth]{caption}
\usepackage{geometry}
 \geometry{
 left=20mm,
 top=20mm,
 right=20mm,
 bottom=20mm
 }
\usepackage[bottom]{footmisc}


\title{Project in Time Series Analysis}
\author{Manu Upadhyaya | nat11mup \\
	Olle Kjellqvist |  tfy13osv}
\date{}
\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage
\section{Introduction}
In this project we modelled and predicted the outdoor temperature in wonderful village Alvesta in Sweden. The temperature predictions were based on local temperature measurements as well as predictions made by SMHI.
We identified models both with and without the predictions supplied by SMHI. Moreover, we implemented recursive versions (Kalman filter) of these two models. In the end of the report we compare the predictive power of the models.
\newpage
\section{Data treatment}
\subsection{Data selection}
We divided the provided data into 3 sets. One modelling set used to identify model parameters, one validation set used to make sure that the models have not been overfitted, and one test set used to evaluate the performance of the models.

\begin{itemize}
	\item \textbf{Modelling set:} 1994: Week 10, monday, 00:00 to Week 19, sunday, 23:00 \\
	\item \textbf{Validation set:} 1994: Week 20, monday, 00:00 to Week 24, sunday, 23:00 \\
	\item \textbf{Test set:} 1994: Week 40, monday, 00:00 to Week 49, sunday, 23:00 \\
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{ProcessPlot}
	\caption{Temperature measurements in Alvesta.}
	\label{fig:ProcessPlot}
\end{figure}
The sets were selected because of being highest quality with respect to outliers and missing values. See figure \ref{fig:ProcessPlot}.
\subsubsection{Exogenous input}
As input signal (assumed known) we chose the predictions delivered by SMHI in Växjö, see figure \ref{fig:InputPlot}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{InputPlot}
	\caption{Temperature predictions in Växjö.}
	\label{fig:InputPlot}
\end{figure}

\subsection{Treatment of missing data}
In the temperature measurements of Alvesta we detected missing samples at 23:00 every day. These missing values were interpolated as the average of the two neighbouring values (22:00 and 00:00).
\subsection{Treatment of outliers}
In order to get a hint as to whether we should consider outliers we plotted the ACF and TACF of the modelling set, see figure \ref{fig:AcfTacf}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AcfTacf}
	\caption{Modelling set: Auto correlation function and the truncated auto correlation function.}
	\label{fig:AcfTacf}
\end{figure}
As they don't differ by much we chose not to treat outliers specifically.

\newpage
\section{Benchmarks}
In time series analysis we get several tools such as the ACF, PACF, whiteness of residuals etc. in order to guide us along the way. But if the model is to be used for prediction, the only thing that actually counts is the predictive power on new data. In order to judge the quality of our predictions we came up with naive solutions which our rigorous models must beat.
\subsection{1-step prediction}
The naive model is a random walk, i.e. 1-step prediction is the current value:
\[
	\hat{y}_{t|t-1} = y_{t-1}
\]
\subsection{8-step prediction}
The naive model:
\[
	\hat{y}_{t|t-8} = y_{t-24}
\]
\subsection{Results}
\begin{table}[H]
\centering
\caption{Variance of the prediction errors using the naive methods.}
\label{tab:bench}
\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var modelling set} & \textbf{Var test set} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{1.292} & 0.3862                \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{6.449} & 9.8168               
\end{tabular}
\end{table}

\newpage
\section{SARIMA}
\subsection{Model}
After identification we ended up with the model\footnote{Regarding $c_1$. Running \texttt{present} on the model shows $\pm \sigma$ and not the approximate $95\%$ confidence interval, which we realized after we ran the model on the test set. For scientific integrity we did dare change our model.}  :
\begin{equation*}
	\begin{aligned}
		M: & & A(z)\nabla_{24}(z)y_t = C(z)e_t
	\end{aligned}
\end{equation*}
where $\nabla_{24}(z) = 1 - z^{-24}$, $A(z)$ and $C(z)$ were identified using Matlab's built in function PEM, see table \ref{tab:A_coeff}.

\begin{table}[H]
\centering
\caption{Identified coefficients and approximate $95 \%$ confidence intervals to the $A$ and $C$ polynomials for the SARIMA model. All unspecified coefficients are 0.}
\label{tab:A_coeff}
\begin{tabular}{l|ll}
	\textbf{Coefficient} & \textbf{Value} & \textbf{Confidence region} \\  \hline
	$a_0$                & 1              & $\pm 0$                    \\  \cline{2-3}
	$a_1$                & $-1.452$       & $\pm 0.0464$               \\ \cline{2-3}
	$a_2$                & $0.509$        & $\pm 0.0458$               \\   \cline{2-3}
	$a_{23}$               & $-0.165$       & $\pm 0.0436$               \\   \cline{2-3}
	$a_{24}$               & $0.129$        & $\pm 0.0432$               \\ \hline
	$c_0$                & 1              & $\pm 0$                    \\   \cline{2-3}
	$c_1$              & $0.0152$       & $\pm 0.0244$               \\   \cline{2-3}
	$c_{24}$               & $0.914$        & $\pm 0.0228$               \\ \hline
\end{tabular}
\end{table}
\subsection{Model validation}
\subsubsection{Residual analysis}
The residual variance was obtained by filtering the modelling data through the inverse model, and identified to $\sigma^2 = 0.3229$. The auto-correlation function, partial auto-correlation function together with a norm/t-distribution-plot are shown in figure \ref{fig:AModRes}. Note that the residuals are t-distributed, which has roughly the same $95\%$ confidence interval as a normal distribution.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AModRes}
	\caption{Residual analysis of the SARIMA model on the modelling set.}
	\label{fig:AModRes}
\end{figure}
\noindent The residuals passes both the \textit{Ljung-Box-Pierce test} and the \textit{Monti test}, and should thus be considered white. 
\subsubsection{Cross-validation}
In order to make sure that the model has not been overfitted we also studied the residuals when the validation set data was filtered through the inverse model. The residuals passed the \textit{Sign change test}, and looks pretty white as seen in figure \ref{fig:AValRes}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AValRes}
	\caption{Residual analysis of the SARIMA model on the validation set.}
	\label{fig:AValRes}
\end{figure}
\subsubsection{Prediction error analysis}

We constructed 1-step and 8-step predictors in accordance with theorem 6.6 in course book. The results for the modelling set can be seen in figure \ref{fig:AModPred} and table \ref{tab:AModPred}. The ACF, PACF, Norm/t-distribution plots of the corresponding prediction errors are shown in figures \ref{fig:AModPred1} and \ref{fig:AModPred8}. We expect a $k$-step prediction error to behave like an MA($k$-1) process. We are thus satisfied with the 1-step prediction error. This expectation is not fulfilled for the 8-step prediction, and there might be room for improvement.

\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AModPred}
	\caption{Modelling set: 1-step and 8-step predictions together with measured values.}
	\label{fig:AModPred}
\end{figure}

\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the modelling set}
\label{tab:AModPred}
\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var SARIMA} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{1.292} & 0.3300                \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{6.449} & 5.0345               
\end{tabular}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AModPred1Anal}
	\caption{Modelling set: Analysis of the 1-step prediction error.}
	\label{fig:AModPred1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{AModPred8Anal}
	\caption{Modelling set: Analysis of the 8-step prediction error.}
	\label{fig:AModPred8}
\end{figure}

\subsection{Test score}
The results of applying the 1-step and the 8-step predictors on the test set are shown in figure \ref{fig:ATestPred} and table \ref{tab:ATestPred}. 
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{ATestPred}
	\caption{Test set: 1-step and 8-step predictions together with measured values.}
	\label{fig:ATestPred}
\end{figure}

\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the test set}
\label{tab:ATestPred}
\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var SARIMA} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{0.3862} & 0.1860               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{9.8168} & 5.3241               
\end{tabular}
\end{table}

\newpage
\section{Model with exogenous input}

\subsection{Model}
After identification we ended up with the model:
\begin{equation*}
	\begin{aligned}
	\mathcal{M}: & & \nabla_{24}y_t &= B(z)\nabla_{24}u_t + \frac{C(z)}{D(z)}e_t
	\end{aligned}
	\label{eq:BMod}
\end{equation*}
where $B(z),\ C(z)$ and $D(z)$ were identified using Matlab's built in function PEM, see table \ref{tab:Bmod}.
\begin{table}[H]
	\centering
	\caption{Identified Box-Jenkins model for Alvesta, together with approximate $95\%$ confidence regions.}
	\label{tab:Bmod}
\begin{tabular}{l|ll}
	\textbf{Coefficient} & \textbf{Value} & \textbf{Confidence region} \\  \hline
	$b_1$                & 0.174              & $\pm 0.066$                    \\  \hline
	$c_0$                & 1              & $\pm 0$                    \\   \cline{2-3}
	$c_{24}$               & $-0.905$        & $\pm 0.0238$               \\ \hline
	$d_0$		& 1		& $\pm 0$ 	\\ \cline{2-3}
	$d_1$ 		& $-1.442$	& $\pm 0.0424$	\\ \cline{2-3}
	$d_2$		& $0.507$	& $\pm 0.0418$	\\ \cline{2-3}
	$d_{23}$	& $-0.157$	& $\pm 0.0438$	\\ \cline{2-3}
	$d_{24}$	& $0.120$	& $\pm 0.0436$	\\ \hline
\end{tabular}
\end{table}
\subsection{Model Validation}
\subsubsection{Residual analysis}
In order to make sure that all structure in the data that can be modelled has been modelled, we analyzed the residuals of the modelling set, see figure \ref{fig:BModRes}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BModRes}
	\caption{Residual analysis of the BJ-model on the modelling set.}
	\label{fig:BModRes}
\end{figure}
\noindent The residuals passed both the \textit{Ljung-Box-Pierce test} and the \textit{Monti test}, and can be considered white.

\subsection{Cross-validation}
In order to ensure that we had not overfitted the model we ran the inverse model on the validation set to perform residual analysis, see figure \ref{fig:BValRes}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BValRes}
	\caption{Residual analysis of the BJ-model on the validation set.}
	\label{fig:BValRes}
\end{figure}
The residual passed the \textit{Sign-Change test} and can thus be considered white.

\subsection{Prediction error analysis}
We constructed 1-step and 8-step predictors in accordance chapter 6.4 in course book. The results for the modelling set can be seen in figure \ref{fig:BModPred} and table \ref{tab:BModPred}. The ACF, PACF, Norm/t-distribution plots of the corresponding prediction errors are shown in figures \ref{fig:BModPred1} and \ref{fig:BModPred8}. We expect a $k$-step prediction error to behave like an MA($k$-1) process. We are thus satisfied with the 1-step prediction error. This expectation is not fulfilled for the 8-step prediction, and there might be room for improvement.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BModPred}
	\caption{Modelling set: 1-step and 8-step predictions together with the measured values for the BJ-model.}
	\label{fig:BModPred}
\end{figure}

\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the modelling set}
\label{tab:BModPred}
\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var Box-Jenkins} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{1.292} & 0.3248                \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{6.449} & 4.512               
\end{tabular}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BModPred1Anal}
	\caption{Modelling set: 1 step prediction error analysis of the BJ-model.}
	\label{fig:BModPred1}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BModPred8Anal}
	\caption{Modelling set: 8 step prediction error analysis of the BJ-model.}
	\label{fig:BModPred8}
\end{figure}


\subsection{Test score}
The results of applying the 1-step and the 8-step predictors on the test set are shown in figure \ref{fig:BTestPred} and table \ref{tab:BTestPred}. 
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{BTestPred}
	\caption{Test set: 1-step and 8-step predictions together with measured values.}
	\label{fig:BTestPred}
\end{figure}
\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the test set.}
\label{tab:BTestPred}
\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var Box-Jenkins} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{0.3862} & 0.1849               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{9.8168} & 4.5729               
\end{tabular}
\end{table}

\newpage
\section{Recursive estimation}
\subsection{No exogenous input}
Using the model structure from our SARIMA model we can rewrite it in the following way:
\begin{equation}
	\begin{aligned}
		\mathcal{M}_1: & & y_t & = -a_1y_{t-1} - a_2y_{t-2} - a_{23}y_{t-23} + (1 - a_{24})y_{t-24} + \ldots \\
		& & & a_1y_{t-25} + a_2y_{t-26} + a_{23}y_{t-47} + a_{24}y_{t-48} + \ldots \\
		& & &  e_t + c_1e_{t-1} + c_{24}e_{t-24}
	\end{aligned}
		\label{eq:M1}
\end{equation}
Define the state vector as $\vb{x}_t = [1\ a_1(t)\ a_2(t)\ a_{23}(t)\ a_{24}(t)\ c_1(t)\ c_{24}(t)]^T$. $\mathcal{M}_1$ can then be expressed in statespace form:
\begin{equation*}
	\begin{aligned}
		\vb{x}_{t+1} & =\vb{x}_t + \vb{e}_{t} \\
		y_t 	& = C_t\vb{x}_t + w_t
	\end{aligned}
	\label{eq:SS1}
\end{equation*}
where $w_t$ comes from $e_t$ and $\vb{e}_t$ is a vector of noise with intensities corresponding to parameter volatility. $C_t$ comes from collecting the terms in equation (\ref{eq:M1}):
\[
	\begin{aligned}
		C_t & = [y_{t-24}\ (-y_{t-1} + y_{t-25}) \ (-y_{t-2} + y_{t-26}) \ \ldots \\
		& (-y_{t-23} + y_{t-47}) \ (-y_{t-24} + y_{t-48}) \ e_{t-1} \ e_{t-24}]
	\end{aligned}
\]

\subsubsection{Predictions}
We constructed 1-step and 8-step predictors in accordance chapter 8.5 in course book. The results for the modelling set can be seen in figure \ref{fig:CModPred} and table \ref{tab:CModPred}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{CModPred}
	\caption{Modelling set: 1-step and 8-step predictions together with the measured values for the No-input kalman filter.}
	\label{fig:CModPred}
\end{figure}
\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the modelling set for no-input kalman filter.}
	\label{tab:CModPred}
	\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var No-X-Kalman} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{1.292} & 0.4367               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{6.449} & 6.0677             
\end{tabular}
\end{table}
The results of applying the 1-step and the 8-step predictors on the test set are shown in figure \ref{fig:CTestPred} and table \ref{tab:CTestPred}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{CTestPred}
	\caption{Test set: 1-step and 8-step predictions together with the measured values for the No-input kalman filter.}
	\label{fig:CTestPred}
\end{figure}
\begin{table}[H]
	\centering
	\caption{Variance of the prediction errors on the test set for no-input kalman filter.}
	\label{tab:CTestPred}
	\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var No-X-Kalman} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{0.3862} & 0.2048               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{9.8168} & 5.4366               
\end{tabular}
\end{table}
\newpage

\subsection{With exogenous input}
Using the model structure from our BJ model we can rewrite it in the following way:
\begin{equation}
	\begin{aligned}
		\mathcal{M}_2: & & y_t  &= -a_1y_{t-1} - a_2y_{t-2} - a_{23}y_{t-23} + (1 - a_{24})y_{t-24} + 
		 a_1y_{t-25} + a_2y_{t-26} + a_{23}y_{t-47} + a_{24}y_{t-48} + \ldots \\
		& & & b_1u_{t-1} + b_2u_{t-2} + b_3u_{t-3} + b_{24}u_{t-24} + 
		(-b_1 + b_{25})u_{t-25} - b_2u_{t-26} - b_3u_{t-27}  - b_{24}u_{t-48} - b_{25}u_{t-49} + \ldots \\
		& & & e_t + c_{24}e_{t-24}
	\end{aligned}
		\label{eq:M2}
\end{equation}

Defining the state vector $x_t$ as
\[
	x_t = [1\ a_1(t)\ a_2(t)\ a_{23}(t)\ a_{24}(t)\ b_1(t)\ b_2(t)\ b_{24}(t)\ b_{25}(t)\ c_{24}(t)]^T
\]
$\mathcal{M}_2$ can be written on statespace form, like equation (\ref{eq:SS1}), with $C_t$ defined by:
\begin{equation*}
	\begin{aligned}
		C_t & =[y_{t-24}\ (-y_{t-1} + y_{t-25})\ (-y_{t-2} + y_{t-26})\ (-y_{t-23} + y_{t-47})\ 
		(-y_{t-24} + y_{t-48})\ \\
		& (u_{t-1} - u_{t-25})\ (u_{t-2} - u_{t-26})\ (u_{t-3} - u_{t-27})\ (u_{t-24} - u_{t-48})\ (u_{t-25} - u_{t-49})\ e_{t-24}]
	\end{aligned}
\end{equation*}

\subsubsection{Predictions}
We constructed 1-step and 8-step predictors in accordance chapter 8.5 in course book. The results for the modelling set can be seen in figure \ref{fig:CModPred2} and table \ref{tab:CModPred2}.
\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{CModPred2}
	\caption{Modelling set: 1-step and 8-step predictions together with the measured values for the kalman filter with exogenous input.}
	\label{fig:CModPred2}
\end{figure}

\begin{table}[H]
\centering
	\caption{Variance of the prediction errors on the modelling set for kalman filter with exogenous input.}
	\label{tab:CModPred2}
	\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var X-Kalman} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{1.292} & 0.3352               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{6.449} & 5.1527          
\end{tabular}
\end{table}

The results of applying the 1-step and the 8-step predictors on the test set are shown in figure \ref{fig:CTestPred2} and table \ref{tab:CTestPred2}.

\begin{figure}[H]
	\centering
	\includegraphics[width = \textwidth]{CTestPred2}
	\caption{Test set: 1-step and 8-step predictions together with the measured values for the kalman filter with exogenous input.}
	\label{fig:CTestPred2}
\end{figure}

\begin{table}[H]
	\centering
	\caption{Variance of the prediction errors on the test set for kalman filter with exogenous input.}
	\label{tab:CTestPred2}
	\begin{tabular}{lll}
	\textbf{k-step}             & \textbf{Var Benchmark} & \textbf{Var X-Kalman} \\ \hline
	\multicolumn{1}{l|}{1-step} & \multicolumn{1}{l|}{0.3862} & 0.2046               \\
	\multicolumn{1}{l|}{8-step} & \multicolumn{1}{l|}{9.8168} & 4.8813               
\end{tabular}
\end{table}

\newpage
\section{Summary and conclusions}
A summary of the test results are shown in table \ref{tab:sum}.
\begin{table}[H]
\centering
\caption{Summary of prediction error variances on the test results}
\label{tab:sum}
\begin{tabular}{lll}
\textbf{Model}                   & \textbf{Test: 1 - step}     & \textbf{Test: 8 - step} \\ \hline
\multicolumn{1}{l|}{Benchmark}   & \multicolumn{1}{l|}{0.3862} & 9.8168                  \\
\multicolumn{1}{l|}{SARIMA}      & \multicolumn{1}{l|}{0.1860} & 5.3241                  \\
\multicolumn{1}{l|}{Box Jenkins} & \multicolumn{1}{l|}{0.1849} & 4.5729                  \\
\multicolumn{1}{l|}{No-X-Kalman} & \multicolumn{1}{l|}{0.2048} & 5.4366                  \\
\multicolumn{1}{l|}{X-Kalman}    & \multicolumn{1}{l|}{0.2046} & 4.8813                 
\end{tabular}
\end{table}
\noindent The best results was obtained with the non-recursive model with exogenous input, so that's the model we would propose to be used for any practical application. We are satisfied that all the models beat the benchmark with a good margin. 
\end{document}
