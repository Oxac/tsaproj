\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Data treatment}{4}
\contentsline {subsection}{\numberline {2.1}Data selection}{4}
\contentsline {subsubsection}{\numberline {2.1.1}Exogenous input}{4}
\contentsline {subsection}{\numberline {2.2}Treatment of missing data}{5}
\contentsline {subsection}{\numberline {2.3}Treatment of outliers}{5}
\contentsline {section}{\numberline {3}Benchmarks}{7}
\contentsline {subsection}{\numberline {3.1}1-step prediction}{7}
\contentsline {subsection}{\numberline {3.2}8-step prediction}{7}
\contentsline {subsection}{\numberline {3.3}Results}{7}
\contentsline {section}{\numberline {4}SARIMA}{8}
\contentsline {subsection}{\numberline {4.1}Model}{8}
\contentsline {subsection}{\numberline {4.2}Model validation}{8}
\contentsline {subsubsection}{\numberline {4.2.1}Residual analysis}{8}
\contentsline {subsubsection}{\numberline {4.2.2}Cross-validation}{9}
\contentsline {subsubsection}{\numberline {4.2.3}Prediction error analysis}{10}
\contentsline {subsection}{\numberline {4.3}Test score}{13}
\contentsline {section}{\numberline {5}Model with exogenous input}{15}
\contentsline {subsection}{\numberline {5.1}Model}{15}
\contentsline {subsection}{\numberline {5.2}Model Validation}{15}
\contentsline {subsubsection}{\numberline {5.2.1}Residual analysis}{15}
\contentsline {subsection}{\numberline {5.3}Cross-validation}{16}
\contentsline {subsection}{\numberline {5.4}Prediction error analysis}{17}
\contentsline {subsection}{\numberline {5.5}Test score}{20}
\contentsline {section}{\numberline {6}Recursive estimation}{22}
\contentsline {subsection}{\numberline {6.1}No exogenous input}{22}
\contentsline {subsubsection}{\numberline {6.1.1}Predictions}{22}
\contentsline {subsection}{\numberline {6.2}With exogenous input}{25}
\contentsline {subsubsection}{\numberline {6.2.1}Predictions}{25}
\contentsline {section}{\numberline {7}Summary and conclusions}{27}
