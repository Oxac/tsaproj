%% Modelling SMHI prediction, atmpt @ prewhitening:

addpath(genpath('Data'))
addpath(genpath('Material/matlab'))
%% Load data
loadData
MSGID = 'Ident:simulink:recursiveEstimationObsoleteReplace';
warning('off', MSGID)
%%
% Classify data
y94 = utempAva_9395(length(tid93)+1:2*length(tid93), end);
y95 = utempAva_9395(2*length(tid93):end, end);
% Interpolate all missing samples at 00:00
k = 24:24:(length(y94)-1);
y94(k') = (y94(k'-1) + y94(k'+1))/2;
k = 25:24:(length(y95)-1);
y95(k') = (y95(k'-1) + y95(k'+1))/2;
y95(1) =(y94(end) + y95(2))/2;

% Exogenus input
x94 = ptvxo94;
x95 = ptvxo95;
figure
stem(crosscorr(x94, y94));
title('Crosscorrelation (entire year 94)')
xlabel('lags')
%%
% Get the right indexes;
modDur = 10;
d2find = [modDur 6 23];
kmod = (1:find(sum(tid94(:, 3:end) == d2find,2) == 3))' + 1500;
kval = kmod(1:floor(length(kmod)/2));
ktest1 = kmod(floor(length(kmod)/2)+1:end);
ktest2 = (find(sum(tid94(:, 3:end) == [20 1 0],2) == 3):find(sum(tid94(:, 3:end) == [24 6 23],2) == 3))' + 1500;
%%
ym = y94(kmod);
yv = y95(kval);         % Validation set (in season)
yt1 = y95(ktest1);      % Test set (in season)
yt2 = y94(ktest2);      % Test set (out of season)
y_mean = mean(ym);
y0 = ym - y_mean;

xm = x94(kmod);
xv = x95(kval);         % Validation set (in season)    (input)
% xt1 = x95(ktest1);      % Test set (in season)          (input)
xt2 = x94(ktest2);      % Test set (out of season)      (input)
x_mean = mean(xm);
x0 = xm - x_mean;

%%
% Model input
[xcf, lags, bnds] = crosscorr(ym, xm, 100, 2);
figure
stem(lags, xcf)
hold on
plot(kron([1 1], [min(lags) max(lags)]'),kron(bnds',[1 1]'), 'b-');
%%
% Nice, this should be helpful
% BENCHMARK!
e_bench = yv- xv;
analRes(e_bench, 200, .05, 1);
norm(e_bench)
%%
% Now model x!
analRes(xm, 200, .05, 1);
figure
subplot(211)
stem([acf(xm, 400) tacf(xm ,400)]);
subplot(212)
stem(acf(xm, 400) - tacf(xm ,400));
%%
% Nothing drastisc!

%%

A = [1 zeros(1,25)];
C = [1 zeros(1,25)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 1 1 zeros(1,20) 1 1 1];
m_init.Structure.a.Free = [1 1 zeros(1,22) 1 1];
modx = pem(x0, m_init);
resx = filter(modx.a, modx.c, x0);
resx = resx(40:end);
analRes(resx, 200, .95, 0);
%%
%% X-validation harr harr
% mod1 gives a better solution.
xv0 = xv - x_mean;
resX1 = filter(modx.a, modx.c, xv0);
resX1= resX1(40:end);
analRes(resX1, 200, .95, 0);

%%
% Prewhitening!

ypw = filter(modx.a, modx.c, y0);
xpw = filter(modx.a, modx.c, x0);

figure
M = 80; stem(-M:M, crosscorr(xpw, ypw, M));
title('CCF');
xlabel('Lag')
hold on
plot(kron((-M:M)',[1 1]), 2/sqrt(length(ypw))*kron(ones(1,2*M+1)',[1 -1]),'--')   % One lining!
hold off
%%
A2 = [1 0];
B =  [1 0];
Mi = idpoly([1], [B], [], [], [A2]);
zpw = iddata(ypw, xpw,1);
pwmod = pem(zpw, Mi); present(pwmod)
vhat = resid(pwmod, zpw);
resid(pwmod, zpw)
%%
% Filter input and subtract!
yhat = y0 - filter(pwmod.b, pwmod.f, x0);
yhat = yhat(3:end);
analRes(yhat, 500);
%%
% I don't get it much better than this...
A = [1 zeros(1,25)];
C = [1 zeros(1,25)];
m_init = idpoly(A, [], C);
m_init.Structure.c.Free = [1 1 1 zeros(1,20) 1 1 1];
m_init.Structure.a.Free = [1 1 zeros(1,22) 1 1];

modhat = pem(yhat, m_init);
reshat = filter(modhat.a, modhat.c, yhat);
analRes(reshat, 200, .05, 0);

%%
% CRUNCH THE NUMBERS, MAKE IT BETTER?
% Now reestimate all parameters!
%
% Define Orders:
D                       = [1 zeros(1,25)];      % Also called A1
F                       = [1 0];                % Also called A2
B                       = [1 0];
C                       = [1 zeros(1,25)];
Mi                      = idpoly(1, B, C, D, F);
Mi.structure.d.Free     = [1 1 zeros(1,22) 1 1];
Mi.structure.c.Free     = [1 1 1 zeros(1,20) 1 1 1];

zm = iddata(ym, xm);
MboxJ = pem(zm, Mi);
present(MboxJ)
ehat = resid(MboxJ, zm);
%%
figure
resid(MboxJ, zm);
analRes(ehat.y, 50, 0.05, 0);
%% Prediction!!!

A_armax = conv(MboxJ.d, MboxJ.f);
B_armax = conv(MboxJ.b, MboxJ.d);
C_armax = conv(MboxJ.c, MboxJ.f);
%%
k = 1;
% For general good measure
[CS, AS] = equalLength(C_armax, A_armax);
[Fk, Gk] = deconv(conv([1, zeros(1,k-1)], CS), AS);

BF = conv(B_armax, Fk);

[CS, BFS] = equalLength(C_armax, BF);
[Fuk, Guk] = deconv(conv([1, zeros(1,k-1)], BFS), CS);

yhat_k = filter(Guk, C, x0) + filter(Gk, C, y0) + filter(Fuk, 1, x0);


n = max([length(Guk) length(C) length(Gk)]);
yhat_k = yhat_k(n+1:end) + y_mean;
ymk = ym(n+1:end);
xmk = xm(n+1:end);
%%
e_benchm = ymk - xmk;
res = ymk - yhat_k;
subplot(211)
plot([yhat_k xmk ymk])
axis('tight')
title('prediction')
legend('p8','xm', 'ym')
subplot(212)
plot([res e_benchm])
title('res')
%%
analRes(res, 200, .05, 0);      % Maybe could use differentiating... fuck
legend('p8', 'benchmark')
axis('tight')

%%
% It doesn't beat benchmark... Maybe differentiating and/or
% transformation might help. Anyway I have an outline.

%%
save('bj.mat', 'A_armax', 'B_armax', 'C_armax', 'y_mean', 'x_mean')